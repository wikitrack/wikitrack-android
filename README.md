# README #

A little handy tool for watching the changes in your favourite wiki projects. For now, we have three features. 

1. Recent Changes
2. User Contributions 
3. Watchlist

The above module works with the diffs module.

### What is this repository for? ###

An app for tracking the wiki projects and wiki users. *Ver 0.1

### Used Library ###
* Pull to Refresh library
https://github.com/chrisbanes/ActionBar-PullToRefresh/wiki/QuickStart-ABC
* Google's ActionBarCompat
* Okhttp
https://github.com/square/okhttp
* Google's GSON
* Google diff-match-patch
https://code.google.com/p/google-diff-match-patch/
* gwtwiki
https://code.google.com/p/gwtwiki/