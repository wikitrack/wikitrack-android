package com.hpnadig.wikitrack;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.hpnadig.wikitrack.events.ProjectChangeEvent;
import com.hpnadig.wikitrack.login.LoginActivity;
import com.hpnadig.wikitrack.recentchanges.ActivityDisplayRecentChangesPages;
import com.hpnadig.wikitrack.usercontribution.UCInputActivity;
import com.hpnadig.wikitrack.watchlist.WatchListActivity;

import org.wikipedia.settings.SettingsActivity;

import java.lang.reflect.Field;
import java.util.List;

/**
 * 
 * Dashboard Activity 
 * 
 * */

public class ActivityDashboard extends AppCompatActivity implements AdapterView.OnItemClickListener{

	
	private String [] menuItemNames = {"Recent Changes","User Contributions", "Watchlist"};
	private int [] menuItemImages = {R.drawable.wt_recent_changes,R.drawable.wt_user_contributions,R.drawable.wt_watchlist};
	TextView tvRecentChanges,tvUserContributions,tvLoginLogout,tvWatchlist;
	WikiTrack app;
	ListView listViewMenu;
	LinearLayout llLogin;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Crashlytics.start(this);
		setContentView(R.layout.activity_dashboard);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
		setSupportActionBar(toolbar);
		app = (WikiTrack)getApplication();
		app.getBus().register(this);

		//showing the overflow button
		try {
		    	ViewConfiguration config = ViewConfiguration.get(this);
		    	Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
		    	if (menuKeyField != null) 
		    	{
		    		menuKeyField.setAccessible(true);
		    		menuKeyField.setBoolean(config, false);
		    	}
		} catch (Exception e) {
		    	e.printStackTrace();
		}
		
		listViewMenu = (ListView)findViewById(R.id.listview_menu);
		listViewMenu.setAdapter(new MenuListAdapter());
		listViewMenu.setOnItemClickListener(this);
		llLogin = (LinearLayout)findViewById(R.id.ll_login);
		tvLoginLogout = (TextView)findViewById(R.id.tv_login);
		llLogin.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AppConstants.log("click event");
				if(app.getUserInfoStorage().isLoggedIn())
				{
					app.getCookieManager().clearAllCookies();
					app.getUserInfoStorage().clearUser();
					tvLoginLogout.setText(getString(R.string.login));
				}else {
					if(app.isOnline())
					{
						Intent login = new Intent(getApplicationContext(),LoginActivity.class);
						startActivity(login);
					}else {
						AppConstants.toast(getApplicationContext(),getString(R.string.message_check_internet));
					}	
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.wiki_base, menu);
		return super.onCreateOptionsMenu(menu);
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		    case android.R.id.home:
		    	onBackPressed();
		    	return true;
		    case R.id.action_fav_project:
		    	 //Show the project names or list, if any
		    	 //If he chooses any project, stored that to preferences and also send a broadcast using BUS
		    	 //to change the content.
		    	 String arr;
		    	 List<String> listFavProject = app.getUserInfoStorage().getFavProject();
		    	 if(listFavProject != null && listFavProject.size()>0) {
			    	 final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,listFavProject);
			    	 AlertDialog.Builder builder = new AlertDialog.Builder(this);
			    	 builder.setTitle(R.string.favourite_project)
			    	           .setAdapter(adapter, new DialogInterface.OnClickListener() {
			    	               public void onClick(DialogInterface dialog, int which) {
			    	            	   AppConstants.log("+++++ list item clicked ++++++");
			    	            	   // Since we'll have to store the project key, not the project name. 
			    	            	   String projectName = adapter.getItem(which);
			    	            	   AppConstants.log("ProjectName : "+projectName);
			    	            	   String [] wikiProjectKeys = getResources().getStringArray(R.array.preference_project_keys);
			    	               	   String [] wikiProjectCanoName = getResources().getStringArray(R.array.preference_project_canonical_name);
			    	               	   for(int i=0;i < wikiProjectCanoName.length;i++)
				    	               {
				    	               	  if(projectName.equals(wikiProjectCanoName[i]))
				    	               	  { 
				    	               		    String projectKey = wikiProjectKeys[i];
				    	               		    AppConstants.log("ProjectKey : "+projectKey);
				    	               			app.getPreferences().edit().putString(AppConstants.WIKI_PROJECT, projectKey).commit();
				    	               			app.getBus().post(new ProjectChangeEvent(projectKey));
				    	               			break;
				    	               	  }
				    	               }
			    	           }
			    	    });
			    	builder.create().show();  
		    	 } else {
		    		 AppConstants.toast(this.getApplicationContext(),getResources().getString(R.string.message_no_fav_projects));
		    	 }
				break;	
			case R.id.action_change_language_project:
				Intent chLanguage = new Intent(this,PreDashboardActivity.class);
				startActivity(chLanguage);
				break;
			case R.id.action_watchlist:
				Intent chProject = new Intent(this,WatchListActivity.class);
				startActivity(chProject);
				break;
			case R.id.action_user_contributions:
				Intent ucUserContribs = new Intent(this,UCInputActivity.class);
				startActivity(ucUserContribs);
				break;
			case R.id.action_recent_changes:
				Intent rcRecentChanges = new Intent(this,ActivityDisplayRecentChangesPages.class);
				startActivity(rcRecentChanges);
				break;
			case R.id.action_settings:
				Intent more = new Intent(this,SettingsActivity.class);
				startActivity(more);
				break;
			default:
				break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onResume() {
		if(app.getUserInfoStorage().isLoggedIn())
		{
			tvLoginLogout.setText(getString(R.string.logout));
		}
		super.onResume();
	}
	
	
	
	@Override
	public void onItemClick(AdapterView<?> adapterView, View row, int position, long id) {
		switch (position) {
		case 0:
			if(app.isOnline())
			{
				Intent rc = new Intent(this,ActivityDisplayRecentChangesPages.class);
				startActivity(rc);
			}else {
				AppConstants.toast(this,getString(R.string.message_check_internet));
			}
			break;
		case 1:
			Intent uc = new Intent(this,UCInputActivity.class);
			startActivity(uc);
			break;

		case 2:
			Intent wl = new Intent(this,WatchListActivity.class);
			startActivity(wl);
			break;
		default:
			break;
		}
	}
	
	class MenuListAdapter extends BaseAdapter{

		@Override
		public int getCount() {
			return menuItemNames.length;
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if(convertView==null)
			{
				convertView = ActivityDashboard.this.getLayoutInflater().inflate(R.layout.layout_listview_item_with_image,parent,false);
			}
			
			TextView name =(TextView)convertView.findViewById(R.id.textViewId);
			ImageView icon = (ImageView)convertView.findViewById(R.id.imageViewId);
			
			name.setText(menuItemNames[position]);
			icon.setImageResource(menuItemImages[position]);
			return convertView;
		}
		
	}

	
	
}
