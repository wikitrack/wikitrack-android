package com.hpnadig.wikitrack;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

/**
 * 
 *    A helper class for showing the logs.
 *    In the production mode, change this debug variable to false. 
 * 
 **/

public class AppConstants {

	private static final boolean debug = true;
	public static final String dateFormat = "yyyy-MM-dd HH:mm a";
	public static final String ERROR_CODE_LOGIN_REQUIRED = "wlnotloggedin";
		
	/**
	 * 
	 * Constants
	 * 
	 * */
	public static final String WIKI_PROJECT="wiki_project";
	public static final String WIKI_LANGUAGE="wiki_language";
	public static final String EXTRA_RC_URL="extra_rc_url";
	public static final String EXTRA_USER_ID="extra_user_id";
	public static final String EXTRA_PAGEID="extra_page_id";
	public static final String EXTRA_PAGETITLE="extra_page_title";
	public static final String EXTRA_USER="extra_user";
	public static final String EXTRA_TIMESTAMP="extra_timestamp";
	public static final String EXTRA_REVID="extra_rev_id";
	
	/**
	 * 
	 *  Activities will call this static method for the logs.
	 * 
	 * */
	
	public static void log(String logMessage)
	{
		if(debug)
			Log.d("debug", logMessage);
	}

	public static void logE(String logMessage, Exception e)
	{
		if(debug)
			Log.e("debug", logMessage, e);
	}
	
	public static void toast(Context ctx, String message)
	{
		Toast.makeText(ctx, message, Toast.LENGTH_SHORT).show();
	}
}
