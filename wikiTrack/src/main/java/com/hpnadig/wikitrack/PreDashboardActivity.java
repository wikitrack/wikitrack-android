package com.hpnadig.wikitrack;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.hpnadig.wikitrack.list.LanguageListActivity;
import com.hpnadig.wikitrack.navdrawer.WikiBaseActivity;

/**
 * <p>
 * Activity for choosing the wiki language and wiki projects. Initially, two buttons are there.
 * </p>
 **/


public class PreDashboardActivity extends WikiBaseActivity implements OnTouchListener{

    private TextView tvChooseLanguage,tvChooseProject;
    private static final int REQUEST_CHANGE_LANGUAGE=0;
    private static final int REQUEST_CHANGE_PROJECT=1;
    private String languageSelected;
    private LinearLayout llchooseLanguage,llchooseProject;
    WikiTrack app;
    String projectSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app =(WikiTrack)getApplication();
        setContentView(R.layout.activity_project_language_selection);
        llchooseLanguage = (LinearLayout)findViewById(R.id.ll_choose_language);
        llchooseProject = (LinearLayout)findViewById(R.id.ll_choose_projects);
        tvChooseLanguage = (TextView)findViewById(R.id.tv_choose_language);
        tvChooseProject = (TextView)findViewById(R.id.tvChooseProject);
        llchooseProject.setOnTouchListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        projectSelected = app.getPreferences().getString(AppConstants.WIKI_PROJECT,null);
        if(projectSelected!=null && app.isMultiLanguageProject(projectSelected))
        {
            tvChooseProject.setText(app.canonicalWikiProjectName(projectSelected));
            llchooseLanguage.setOnTouchListener(this);
            llchooseLanguage.setBackgroundResource(R.color.background_gray);
        } else {
            String message  = getString(R.string.message_language_selected)+": "+"English";
            tvChooseLanguage.setText(message);
            llchooseLanguage.setOnTouchListener(null);
            llchooseLanguage.setBackgroundResource(R.color.dark_gray);
        }
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if(event.getAction() == MotionEvent.ACTION_DOWN)
        {
            switch (v.getId()) {
                case R.id.ll_choose_language:
                    Intent i = new Intent(this,LanguageListActivity.class);
                    startActivityForResult(i,REQUEST_CHANGE_LANGUAGE);
                    break;
                case R.id.ll_choose_projects:
                    changeProject();
                    break;
                default:
                    break;
            }
            return true;
        }
        return false;
    }


    @Override
    protected void onActivityResult(int reqCode, int resCode, Intent courierService) {
        if(reqCode == REQUEST_CHANGE_LANGUAGE) {
            if(resCode == RESULT_OK) {
                resetLogin();
                languageSelected = courierService.getStringExtra(WikiTrack.PREFERENCE_CONTENT_LANGUAGE);
                String message  = getString(R.string.message_language_selected)+": "+languageSelected;
                tvChooseLanguage.setText(message);
                message = getString(R.string.message_select)+" <b>"+ app.canonicalWikiProjectName(projectSelected) +"</b> in <b>"+languageSelected +  "</b>.  "+getString(R.string.message_remember_language_n_project);
                showPopUp(Html.fromHtml(message).toString());
            }
        }else if(reqCode == REQUEST_CHANGE_PROJECT) {
            if(resCode == RESULT_OK) {
                resetLogin();
                projectSelected = app.getPreferences().getString(AppConstants.WIKI_PROJECT,"wikipedia");
                String projectSelectedName = courierService.getStringExtra("PROJECT_NAME_SELECTED");
                tvChooseProject.setText(projectSelectedName);

                if(projectSelected!=null && app.isMultiLanguageProject(projectSelected)) {
                    llchooseLanguage.setOnTouchListener(this);
                    llchooseLanguage.setBackgroundResource(R.color.background_gray);
                } else {
                    String message  = getString(R.string.message_language_selected)+": "+"English";
                    tvChooseLanguage.setText(message);
                    llchooseLanguage.setOnTouchListener(null);
                    llchooseLanguage.setBackgroundResource(R.color.dark_gray);
                    message =getString(R.string.message_select)+" "+app.canonicalWikiProjectName(projectSelected)+" "+getString(R.string.message_remember_language_n_project_main);
                    showPopUp(message);
                }
            }
        }
    }

    private void changeProject()
    {
        Intent intent = new Intent(this,ProjectListActivity.class);
        intent.putExtra(AppConstants.WIKI_LANGUAGE,languageSelected);
        startActivityForResult(intent,REQUEST_CHANGE_PROJECT);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.ic_launcher);
        builder.setMessage(getString(R.string.message_confirm_at_exit));
        builder.setPositiveButton(getString(R.string.message_confirm_at_exit_no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        Intent showDashboard = new Intent(getApplicationContext(),ActivityDashboard.class);
                        startActivity(showDashboard);
                    }
                });

        builder.setNegativeButton(getString(R.string.message_confirm_at_exit_yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                    }
                });
        builder.show();
    }

    private void showPopUp(String message)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.ic_launcher);
        builder.setMessage(message);
        builder.setNegativeButton(getString(R.string.btn_skip),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        Intent showDashboard = new Intent(getApplicationContext(),ActivityDashboard.class);
                        startActivity(showDashboard);
                    }
                });
        builder.setPositiveButton(getString(R.string.btn_remember),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        Intent showDashboard = new Intent(getApplicationContext(),ActivityDashboard.class);
                        startActivity(showDashboard);
                        app.getUserInfoStorage().addFavProject(app.canonicalWikiProjectName(projectSelected));
                    }
                });
        builder.show();
    }

    private void resetLogin() {
        app.getCookieManager().clearAllCookies();
        app.getUserInfoStorage().clearUser();
    }
}
