package com.hpnadig.wikitrack;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.hpnadig.wikitrack.navdrawer.WikiBaseActivity;

public class ProjectListActivity extends WikiBaseActivity implements OnItemClickListener{

    private ListView listView;
    private String[] wikiProjects;
    private WikiTrack app;
    ProjectListAdapter adapter;
    private int wikiProjectImages[]={R.drawable.wikipedia,R.drawable.wiktionary,R.drawable.news,R.drawable.wikisppecies,R.drawable.wikiquote,
            R.drawable.commons,R.drawable.wikibooks,R.drawable.wikivoyage,R.drawable.incubator,R.drawable.meta};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.template_listview);

        app =(WikiTrack)getApplication();
        wikiProjects = getResources().getStringArray(R.array.preference_project_canonical_name);
        SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_layout);
        swipeRefreshLayout.setEnabled(false);
        ProgressBar progressBar = (ProgressBar)findViewById(R.id.progress_bar_list);
        progressBar.setVisibility(View.GONE);
        listView = (ListView)findViewById(R.id.listview);
        adapter = new ProjectListAdapter();
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onItemClick(AdapterView<?> listView, View row, int position, long id) {
        String wikiProjectName = adapter.getItem(position);
        String wikiProjectKey = getProjectKey(wikiProjectName);
        app.getPreferences().edit().putString(AppConstants.WIKI_PROJECT,wikiProjectKey).commit();


        Intent courierService = new Intent();
        courierService.putExtra("PROJECT_NAME_SELECTED",wikiProjectName);
        setResult(RESULT_OK, courierService);
        finish();
    }

    private void showPopUp(String message)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.ic_launcher);
        builder.setMessage(message);
        builder.setNegativeButton(getString(R.string.app_name),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                        Intent showDashboard = new Intent(getApplicationContext(),ActivityDashboard.class);
                        startActivity(showDashboard);
                    }
                });
        builder.setPositiveButton("Remember",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                        Intent showDashboard = new Intent(getApplicationContext(),ActivityDashboard.class);
                        startActivity(showDashboard);
                    }
                });


        builder.show();
    }

    private String getProjectKey(String projectName)
    {
        String [] arrProjectKeys =getResources().getStringArray(R.array.preference_project_keys);
        return arrProjectKeys[getProjectIndex(projectName)];
    }

    private int getProjectIndex(String projectName)
    {
        String [] p =getResources().getStringArray(R.array.preference_project_canonical_name);
        for(int i=0;i<p.length;i++)
        {
            if(projectName.equals(p[i]))
                return i;
        }
        return 0;
    }


    class ProjectListAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return wikiProjects.length;
        }

        @Override
        public String getItem(int position) {
            return wikiProjects[position];
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView==null)
            {
                convertView = getLayoutInflater().inflate(R.layout.layout_listview_item_with_image,parent,false);
            }

            TextView name =(TextView)convertView.findViewById(R.id.textViewId);
            ImageView icon = (ImageView)convertView.findViewById(R.id.imageViewId);

            name.setText(wikiProjects[position]);
            icon.setImageResource(wikiProjectImages[position]);
            return convertView;
        }

    }


}

