package com.hpnadig.wikitrack;

import java.util.Arrays;
import android.annotation.SuppressLint;
import android.os.Build;
import android.view.View;


public class Utils {

	/**
     * Takes a language code (as returned by Android) and returns a wiki code, as used by wikipedia.
     *
     * @param langCode Language code (as returned by Android)
     * @return Wiki code, as used by wikipedia.
     */
    public static String langCodeToWikiLang(String langCode) {
        // Convert deprecated language codes to modern ones.
        // See https://developer.android.com/reference/java/util/Locale.html
        if (langCode.equals("iw")) {
            return "he"; // Hebrew
        } else if (langCode.equals("in")) {
            return "id"; // Indonesian
        } else if (langCode.equals("ji")) {
            return "yi"; // Yiddish
        }
        return langCode;
    }
    
    /**
     * Sets text direction (RTL / LTR) for given view based on given lang.
     *
     * Doesn't do anything on pre Android 4.2, since their RTL support is terrible.
     *
     * @param view View to set direction of
     * @param lang Wiki code for the language based on which to set direction
     */
    @SuppressLint("NewApi") 
    public static void setTextDirection(View view, String lang) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
        	AppConstants.log("isRTL : "+Utils.isLangRTL(lang));
            //view.setTextDirection(Utils.isLangRTL(lang) ? View.TEXT_DIRECTION_RTL : View.TEXT_DIRECTION_LTR);
           // view.setTextAlignment(Utils.isLangRTL(lang) ? View.TEXT_ALIGNMENT_TEXT_END : View.TEXT_ALIGNMENT_TEXT_START);
            view.setLayoutDirection(Utils.isLangRTL(lang)?View.LAYOUT_DIRECTION_RTL:View.LAYOUT_DIRECTION_LTR);
        }
    }
    

    /**
     * List of wiki language codes for which the content is primarily RTL.
     *
     * Ensure that this is always sorted alphabetically.
     */
    private static final String[] RTL_LANGS = {
            "ar", "arc", "arz", "bcc", "bqi", "ckb", "dv", "fa", "glk", "ha", "he",
            "khw", "ks", "mzn", "pnb", "ps", "sd", "ug", "ur", "yi"
    };

    /**
     * Returns true if the given wiki language is to be displayed RTL.
     *
     * @param lang Wiki code for the language to check for directionality
     * @return true if it is RTL, false if LTR
     */
    
    public static boolean isLangRTL(String lang) {
        return Arrays.binarySearch(RTL_LANGS, lang, null) >= 0;
    }

    
    
}
