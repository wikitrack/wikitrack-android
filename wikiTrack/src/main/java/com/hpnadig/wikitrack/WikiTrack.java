package com.hpnadig.wikitrack;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.preference.PreferenceManager;

import com.hpnadig.wikitrack.login.UserInfoStorage;
import com.squareup.otto.Bus;

import org.mediawiki.api.json.Api;

import java.util.HashMap;
import java.util.Locale;



/**
 *  Entry point or gateway of the this application
 *
 *  A class that represents center part of the WikiTrack application. Here we'll declare
 *  variables and methods that are required be the whole application.
 *
 */
public class WikiTrack extends Application{
	
	/**
	 * 
	 *  Initialize the variables that will be accessed across the application.
	 */
	private SharedPreferences prefs;
    public static boolean FALLBACK = false;
    public static String PROTOCOL = "https";
    public static String PREFERENCE_CONTENT_LANGUAGE;
    public static String APP_VERSION_STRING;
    public static String PREFERENCE_COOKIE_DOMAINS;
    public static String PREFERENCE_COOKIES_FOR_DOMAINS;


    /**
     * Initialize the Wikipedia API
     * see json-1.0.jar for more details.
     *
     */
	@Override
	public void onCreate() {
		super.onCreate();
		 	PREFERENCE_CONTENT_LANGUAGE = getResources().getString(R.string.preference_key_language);
	        PREFERENCE_COOKIE_DOMAINS = getString(R.string.preference_cookie_domains);
	        PREFERENCE_COOKIES_FOR_DOMAINS = getString(R.string.preference_cookies_for_domain);
	        PROTOCOL = "https";
		    Api.setConnectionFactory(new OkHttpConnectionFactory(this));
	}

	/**
	 * 
	 * Public method for returning the shared preferences 
	 * 
	 **/
	private Bus bus;
	public Bus getBus()
	{
		if(bus== null)
			bus = new Bus();
		return bus;
	}
	
	public SharedPreferences getPreferences()
	{
		if (prefs == null) {
			return PreferenceManager.getDefaultSharedPreferences(this);
		} else {
			return prefs;
		}
	}
	
	private Site primarySite;
    /**
     * Default site of the application
     * You should use PageTitle.getSite() to get the currently browsed site
     */
    public Site getPrimarySite() {
    	String wikiproject = getPreferences().getString(AppConstants.WIKI_PROJECT,null);
    	if(wikiproject!=null)
    	{
    		boolean flag = isMultiLanguageProject(wikiproject);
    		if(!flag)
    		{
    		  return new Site(getWikiProject()+".wikimedia.org");
    		}
    	}
        if (primarySite == null) {
            primarySite = new Site(getPrimaryLanguage()+"."+getWikiProject()+".org");
        }
        return primarySite;
    }
	
    private String primaryLanguage;
    /**
     * Returns the primary language of the application.
     *
     */
    public String getPrimaryLanguage() {
        if (primaryLanguage == null) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            primaryLanguage = prefs.getString(PREFERENCE_CONTENT_LANGUAGE, null);
            if (primaryLanguage == null) {
                // No preference set!
                String wikiCode = Utils.langCodeToWikiLang(Locale.getDefault().getLanguage());
                if (!isWikiLanguage(wikiCode)) {
                    wikiCode = "en"; // fallback, see comments in #findWikiIndex
                }
                return wikiCode;
            }
        }
        return primaryLanguage;
    }

    /**
     * Sets the primary language.
     */
    public void setPrimaryLanguage(String language) {
        primaryLanguage = language;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefs.edit().putString(PREFERENCE_CONTENT_LANGUAGE, language).commit();
        primarySite = null;
    }
    
    private String[] canonicalNames;
    public String canonicalNameFor(int index) {
        if (canonicalNames == null) {
            canonicalNames = getResources().getStringArray(R.array.preference_language_canonical_names);
        }
        return canonicalNames[index];
    }
    
    private boolean isWikiLanguage(String lang) {
        if (wikiCodes == null) {
            wikiCodes = getResources().getStringArray(R.array.preference_language_keys);
        }

        for (String wikiCode : wikiCodes) {
            if (wikiCode.equals(lang)) {
                return true;
            }
        }
        return false;
    }


    private HashMap<String, Api> apis = new HashMap<String, Api>();
    public Api getAPIForSite(Site site) {
    	Api api =new Api(site.getApiDomain());
    	return api;
    }
    
    private String userAgent;
    public String getUserAgent() {
        if (userAgent == null) {
            userAgent = String.format("Wikitrack/%s (Android %s; %s)",
                    WikiTrack.APP_VERSION_STRING,
                    Build.VERSION.RELEASE,
                    getString(R.string.device_type
                    ));
        }
        return userAgent;
    }
    
    private UserInfoStorage userInfoStorage;
    public UserInfoStorage getUserInfoStorage() {
        if (userInfoStorage == null) {
            userInfoStorage = new UserInfoStorage(PreferenceManager.getDefaultSharedPreferences(this));
        }
        return userInfoStorage;
    }
    
    private SharedPreferenceCookieManager cookieManager;
    public SharedPreferenceCookieManager getCookieManager() {
        if (cookieManager == null) {
            cookieManager = new SharedPreferenceCookieManager(PreferenceManager.getDefaultSharedPreferences(this));
        }
        return cookieManager;
    }



    private String[] wikiCodes;
    /**
     * Returns the wiki index for the wiki code that further will be used for finding the localname
     * of the language by presenting the index.
     *
     * {@link com.hpnadig.wikitrack.WikiTrack#localNameFor(int)}
     *
     */
    public int findWikiIndex(String wikiCode) {
        if (wikiCodes == null) {
            wikiCodes = getResources().getStringArray(R.array.preference_language_keys);
        }
        for (int i = 0; i < wikiCodes.length; i++) {
            if (wikiCodes[i].equals(wikiCode)) {
                return i;
            }
        }

        // FIXME: Instrument this with EL to find out what is happening on places where there is a lang we can't find
        // In the meantime, just fall back to en. See https://bugzilla.wikimedia.org/show_bug.cgi?id=66140
        return findWikiIndex("en");
    }
    
    private String[] localNames;
    /**
     * Returns the String with local name of the language for the given index.
     * Check the my comments at the top of array for more details.
     */
    public String localNameFor(int index) {
        if (localNames == null) {
            localNames = getResources().getStringArray(R.array.preference_language_local_names);
        }
        return localNames[index];
    }


    private String[] wikiProjectKeys;
    private String[] wikiProjectCanoName;
    /**
     * It checks the string array for getting the canonical name.
     *
     * @return Returns the canonical name of the given project key.
     *
     */
    public String canonicalWikiProjectName(String projectKey)
    {
    	wikiProjectKeys = getResources().getStringArray(R.array.preference_project_keys);
    	wikiProjectCanoName = getResources().getStringArray(R.array.preference_project_canonical_name);
    	for(int i=0;i<wikiProjectKeys.length;i++)
    	{
    		if(projectKey.equals(wikiProjectKeys[i]))
    		{
    			return wikiProjectCanoName[i];
    		}
    	}
    	return null;
    }

    /**
     * This checks within the array which only contains single language projects. If the given project
     * is listed below then it will return false otherwise true.
     *
     * @return  Returns whether a particular project is multi lingual or not.
     */
    public boolean isMultiLanguageProject(String projectName)
	{
		String [] singleLanguageProject = getResources().getStringArray(R.array.preference_singlelanguage_projects);
		for (String project :singleLanguageProject) {
			if(projectName.equals(project))
			{
				return false;
			}
		}
		return true;
	}

    /**
     * Returns the currently selected Wiki project. Default is the Wikipedia
     *
     */
    public String getWikiProject()
	{
		return this.getPreferences().getString(AppConstants.WIKI_PROJECT,"wikipedia");
	}

    /**
     * Returns weather the user is online or not.
     */
	public boolean isOnline()
	{
		ConnectivityManager conMgr = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
	    if(netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()){
	        return false;
	    }
	    return true; 
	}
}
