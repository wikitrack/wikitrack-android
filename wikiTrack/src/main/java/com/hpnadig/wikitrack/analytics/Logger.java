package com.hpnadig.wikitrack.analytics;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.wikipedia.concurrency.SaneAsyncTask;

import android.content.Context;

import com.hpnadig.wikitrack.AppConstants;
import com.hpnadig.wikitrack.R;


public class Logger {
	
	private Context mContext;
	
	
	public Logger(Context pContext) {
		mContext = pContext;
	}
	
	public void log(String username,String project, String language)
	{
		String jsonNode = getJsonString(username, project, language);
		new AsynLogger(jsonNode).execute();
	}
	
	private String getJsonString(String username,String project, String language)
	{
		try {
			
				AppConstants.log(username+"   "+project+"  "+language);
				JSONObject parent=new JSONObject();
				parent.put("type","wikitrack");
				parent.put("title","wikitrack");
				
				JSONObject j1=new JSONObject();
				j1.put("value",username);
				JSONArray a1=new JSONArray();
				a1.put(0, j1);
				JSONObject j2=new JSONObject();
				j2.put("und",a1);
				parent.put("field_user_name",j2);
				
				JSONObject j3=new JSONObject();
				j3.put("value",project);
				JSONArray a2=new JSONArray();
				a2.put(0, j3);
				JSONObject j4=new JSONObject();
				j4.put("und",a2);
				parent.put("field_project_name",j4);
				
				JSONObject j5=new JSONObject();
				j5.put("value",language);
				JSONArray a3=new JSONArray();
				a3.put(0, j5);
				JSONObject j6=new JSONObject();
				j6.put("und",a3);
				parent.put("field_language",j6);
				
				return parent.toString();
				
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		return null;
	}
	
	private class AsynLogger extends SaneAsyncTask<Integer>
	{
		private final String jsonData;
		
		public AsynLogger(String jsonData) {
			super(SINGLE_THREAD);
			this.jsonData = jsonData;
		}

		@Override
		public Integer performTask() throws Throwable {
			try {
				
				HttpClient hc1 = new DefaultHttpClient();
				HttpPost post1 = new HttpPost(mContext.getResources().getString(R.string.site_token_url));
				HttpResponse res1 = hc1.execute(post1);
				String token  = EntityUtils.toString(res1.getEntity());					
				
				
				HttpPost post2 = new HttpPost(mContext.getResources().getString(R.string.site_login_url));
				post2.setHeader("Content-type", "application/json");
				JSONObject obj = new JSONObject();
				obj.put("username", mContext.getResources().getString(R.string.site_username));
				obj.put("password",mContext.getResources().getString(R.string.site_password));
				
			    StringEntity se = new StringEntity(obj.toString()); 
			    se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			    post2.setEntity(se); 
			    HttpResponse response = hc1.execute(post2);
			    String temp = EntityUtils.toString(response.getEntity());		    
			    
				
			    JSONObject jsonResponse = new JSONObject(temp);
				String ses=jsonResponse.getString("sessid");
				String ses_name=jsonResponse.getString("session_name");
			    token = jsonResponse.getString("token");
				String cookie=ses_name + "=" + ses;			    
			    
			    HttpPost postReq=new HttpPost(mContext.getString(R.string.site_push_create_node_url));
				postReq.setHeader("Cookie",cookie);
				postReq.setHeader("Content-type", "application/json");
				postReq.setEntity(new StringEntity(jsonData));
				postReq.setHeader("X-CSRF-Token",token);
				
				response = hc1.execute(postReq);
				String responseString = EntityUtils.toString(response.getEntity());
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}	
		
	}
	
	
}
