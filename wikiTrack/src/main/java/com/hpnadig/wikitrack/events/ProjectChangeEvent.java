package com.hpnadig.wikitrack.events;

public class ProjectChangeEvent {
	private String projectKey;
	
	public ProjectChangeEvent(String projectKey) {
		this.projectKey = projectKey;
	}
	
	public String getProjectKey() {
		return projectKey;
	}
}
