package com.hpnadig.wikitrack.list;

import java.util.List;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public abstract class CommonAdapter extends BaseAdapter{

	LayoutInflater inflater;
	List<? extends IListItem> list;
	
	
	public CommonAdapter(List<? extends IListItem> list, Context ctx) {
		this.list = list;
		inflater = LayoutInflater.from(ctx);
	}
	
	@Override
	public int getCount() {
		if(list!=null)
		   return list.size();
		else return 0;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}
 
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return null;
	}
}
