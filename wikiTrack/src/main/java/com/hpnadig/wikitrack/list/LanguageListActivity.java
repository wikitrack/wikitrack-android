package com.hpnadig.wikitrack.list;

import java.util.ArrayList;
import java.util.Arrays;
import com.hpnadig.wikitrack.AppConstants;
import com.hpnadig.wikitrack.R;
import com.hpnadig.wikitrack.WikiTrack;
import com.hpnadig.wikitrack.navdrawer.WikiBaseActivity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


public class LanguageListActivity extends WikiBaseActivity{

	private ListView languagesList;
    private EditText languagesFilter;
    private String[] languages;
    private WikiTrack app;
    private String projectSelected;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_preference_languages);
	    app = (WikiTrack)getApplicationContext();
        languagesFilter = (EditText) findViewById(R.id.preference_languages_filter);
        languagesList = (ListView) findViewById(R.id.preference_languages_list);
	}

	@Override
	protected void onResume() {
		super.onResume();
		projectSelected = app.getPreferences().getString(AppConstants.WIKI_PROJECT, "wikipedia");
		String arrayName = "preference_"+projectSelected+"_language_keys";
        AppConstants.log("arrayName ==> "+arrayName);
		int intArrayHolder = getResources().getIdentifier(arrayName, "array", app.getPackageName());
		languages = getResources().getStringArray(intArrayHolder);
		
		languagesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String langSelected = (String) languagesList.getAdapter().getItem(i);
                String projSelected = app.getPreferences().getString(AppConstants.WIKI_PROJECT,"wikipedia");
                AppConstants.log("Language "+ langSelected+"  Project "+projSelected);
                boolean isSupported = isSupports(projSelected, langSelected);
                AppConstants.log("isSupported :: "+isSupported);
                
                app.setPrimaryLanguage(langSelected);
                int langIndex = app.findWikiIndex(app.getPrimaryLanguage());
                String canonicalName = app.localNameFor(langIndex);
                String message = getString(R.string.message_select)+" "+canonicalName;
                Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
                
                //Send the selected Language as the result to the calling activity
                String localName = app.localNameFor(langIndex);
                Intent courierService = new Intent();
                courierService.putExtra(WikiTrack.PREFERENCE_CONTENT_LANGUAGE,localName);
                setResult(RESULT_OK, courierService);                
                finish();
            }
        });
        languagesList.setAdapter(new LanguagesAdapter(languages, app));
        int selectedLangIndex = Arrays.asList(languages).indexOf(app.getPrimaryLanguage());
        languagesList.setItemChecked(selectedLangIndex, true);
        languagesList.setSelection(selectedLangIndex - 1);

        languagesFilter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                ((LanguagesAdapter) languagesList.getAdapter()).setFilterText(s.toString());
            }
        });
        
	}
	
	
	private static final class LanguagesAdapter extends BaseAdapter {
        private final String[] originalLanguages;
        private final ArrayList<String> languages;
        private final WikiTrack app;

        private LanguagesAdapter(String[] languages, WikiTrack app) {
            this.originalLanguages = languages;
            this.languages = new ArrayList(Arrays.asList(languages));
            this.app = app;
        }

        public void setFilterText(String filter) {
            this.languages.clear();
            filter = filter.toLowerCase();
            for (String s: originalLanguages) {
                int langIndex = app.findWikiIndex(s);
                String canonicalLang = app.canonicalNameFor(langIndex);
                String localLang = app.localNameFor(langIndex);
                if (s.contains(filter)
                        || canonicalLang.toLowerCase().contains(filter)
                        || localLang.toLowerCase().contains(filter)) {
                    this.languages.add(s);
                }
            }
            notifyDataSetInvalidated();
        }

        @Override
        public int getCount() {
            return languages.size();
        }

        @Override
        public Object getItem(int position) {
            return languages.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.simple_list_item_activated_2, parent, false);
            }

            TextView nameText = (TextView) convertView.findViewById(android.R.id.text1);
            TextView localNameText = (TextView) convertView.findViewById(android.R.id.text2);

            String wikiCode = (String) getItem(position);

            int langIndex = app.findWikiIndex(wikiCode);

            nameText.setText(app.canonicalNameFor(langIndex));
            localNameText.setText(app.localNameFor(langIndex));
            return convertView;
        }
    }
	
	/**
	 * Checks whether that a particular wiki projects support the given Language.
	 * @param project  Project within which to check
	 * @param language  Language to check in above projects
	 * 
	 * @return isSupported  
	 * */
	private boolean isSupports(String project,String lang)
	{
		try {
				int intArrayHolder = getResources().getIdentifier(project,"array",app.getPackageName());
				String[] languages = getResources().getStringArray(intArrayHolder);
				for(String l :languages)
				{
					if(l.equals(lang))
					{
						return true;
					}
				}
				return false;
		} catch (Exception e) {
			return false;
		}
	}
}
