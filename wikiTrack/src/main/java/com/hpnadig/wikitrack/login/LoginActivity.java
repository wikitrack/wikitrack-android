package com.hpnadig.wikitrack.login;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.hpnadig.wikitrack.AppConstants;
import com.hpnadig.wikitrack.NonEmptyValidator;
import com.hpnadig.wikitrack.R;
import com.hpnadig.wikitrack.WikiTrack;
import com.hpnadig.wikitrack.analytics.Logger;



public class LoginActivity extends ActionBarActivity implements OnClickListener{
    public static final int REQUEST_LOGIN = 1;

    public static final int RESULT_LOGIN_SUCCESS = 1;
    public static final int RESULT_LOGIN_FAIL = 2;

    public static final String LOGIN_REQUEST_SOURCE = "login_request_source";
    public static final String EDIT_SESSION_TOKEN = "edit_session_token";
    public static final String ACTION_CREATE_ACCOUNT = "action_create_account";

    private EditText usernameText;
    private EditText passwordText;
    private Button loginButton;

    WikiTrack app;
    ProgressDialog dialog;
    private NonEmptyValidator nonEmptyValidator;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (WikiTrack)getApplicationContext();
        dialog = new ProgressDialog(this);
        setContentView(R.layout.activity_login);

        usernameText = (EditText) findViewById(R.id.login_username_text);
        passwordText = (EditText) findViewById(R.id.login_password_text);

        loginButton = (Button) findViewById(R.id.login_button);
        loginButton.setOnClickListener(this);

        nonEmptyValidator = new NonEmptyValidator(new NonEmptyValidator.ValidationChangedCallback() {
            @Override
            public void onValidationChanged(boolean isValid) {
                loginButton.setEnabled(isValid);
            }
        }, usernameText, passwordText);

        passwordText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    doLogin();
                    return true;
                }
                return false;
            }
        });
        // Assume no login by default
        setResult(RESULT_LOGIN_FAIL);
    }

    private void doLogin() {
        showDialog();
        final String username = usernameText.getText().toString();
        final String password = passwordText.getText().toString();
        new LoginTask(this, app.getPrimarySite(), username, password) {
            @Override
            public void onBeforeExecute() {

            }

            @Override
            public void onCatch(Throwable caught) {
                Log.d("Wikipedia", "Caught " + caught.toString());
            }

            @Override
            public void onFinish(LoginResult result) {
                super.onFinish(result);
                hideDialog();
                if (result.getCode().equals("Success")) {
                    Logger l = new Logger(getApplicationContext());
                    l.log(app.getUserInfoStorage().getUser().getUsername(),app.getWikiProject(),app.getPrimaryLanguage());
                    setResult(RESULT_OK);
                    finish();
                } else {
                    handleError(result.getCode());
                }

            }
        }.execute();
    }

    private void handleError(String result) {
        if (result.equals("WrongPass")) {
            passwordText.requestFocus();
            passwordText.setError(getString(R.string.login_error_wrong_password));
        } else if (result.equals("NotExists")) {
            usernameText.requestFocus();
            usernameText.setError(getString(R.string.login_error_wrong_username));
        } else if (result.equals("Blocked")) {
            Toast.makeText(getApplicationContext(),R.string.login_error_blocked,Toast.LENGTH_SHORT).show();
        } else if (result.equals("Throttled")) {
            Toast.makeText(getApplicationContext(),R.string.login_error_throttled,Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(),R.string.login_error_unknown,Toast.LENGTH_SHORT).show();
            AppConstants.log("Login failed with result " + result);
        }
    }

    @Override
    public void onClick(View v) {
        doLogin();
    }

    private void showDialog()
    {
        dialog.setMessage("Please wait...");
        dialog.show();
    }

    private void hideDialog()
    {
        if(dialog.isShowing())
            dialog.dismiss();
    }

}