package com.hpnadig.wikitrack.login;

import android.content.Context;
import org.json.JSONObject;
import org.mediawiki.api.json.Api;
import org.mediawiki.api.json.ApiResult;
import org.wikipedia.concurrency.SaneAsyncTask;
import com.hpnadig.wikitrack.Site;
import com.hpnadig.wikitrack.WikiTrack;



public class LoginTask extends SaneAsyncTask<LoginResult> {
	
    private final String username;
    private final String password;
    private final Api api;
    private final WikiTrack app;

    public LoginTask(Context context, Site site, String username, String password) {
        super(SINGLE_THREAD);
        app = (WikiTrack)context.getApplicationContext();
        api = app.getAPIForSite(site);
        this.username = username;
        this.password = password;
    }

    @Override
    public void onFinish(LoginResult result) {
        if (result.getCode().equals("Success")) {
            // Set userinfo
            app.getUserInfoStorage().setUser(result.getUser());
        }
    }

    @Override
    public LoginResult performTask() throws Throwable {
    	ApiResult preReq = api.action("login")
                .param("lgname", username)
                .param("lgpassword", password)
                .post();
        JSONObject preReqResult = preReq.asObject();
        String token = preReqResult.optJSONObject("login").optString("token");
        
       ApiResult req = api.action("login")
                .param("lgname", username)
                .param("lgpassword", password)
                .param("lgtoken", token)
                .post();

        JSONObject result = req.asObject().optJSONObject("login");
        User user = null;
        if (result.optString("result").equals("Success")) {
        	user = new User(result.optString("lgusername"), password, result.optInt("lguserid"));
        }
    	return new LoginResult(result.optString("result"), user);
    }
}
