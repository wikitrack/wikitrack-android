package com.hpnadig.wikitrack.login;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import com.hpnadig.wikitrack.AppConstants;
import android.content.SharedPreferences;
import android.text.TextUtils;



public class UserInfoStorage {
	
    private static final String PREFERENCE_USERNAME = "username";
    private static final String PREFERENCE_PASSWORD = "password";
    private static final String PREFERENCE_USERID = "userID";
    private static final String PREFERENCE_FAV_PROJECT = "fav_projects";

    private final SharedPreferences prefs;
    private User currentUser;
    
    public UserInfoStorage(SharedPreferences prefs) {
        this.prefs = prefs;
    }

    public boolean isLoggedIn() {
        return getUser() != null;
    }

    public void setUser(User user) {
        prefs.edit()
                .putString(PREFERENCE_USERNAME, user.getUsername())
                .putString(PREFERENCE_PASSWORD, user.getPassword())
                .putInt(PREFERENCE_USERID, user.getUserID())
                .commit();
    }

    public User getUser() {
        if (currentUser == null) {
            if (prefs.contains(PREFERENCE_USERNAME) && prefs.contains(PREFERENCE_PASSWORD)) {
                currentUser = new User(
                        prefs.getString(PREFERENCE_USERNAME, null),
                        prefs.getString(PREFERENCE_PASSWORD, null),
                        prefs.getInt(PREFERENCE_USERID, 0)
                );
            }
        }
        return currentUser;
    }

    public void clearUser() {
        prefs.edit()
                .remove(PREFERENCE_USERNAME)
                .remove(PREFERENCE_PASSWORD)
                .remove(PREFERENCE_USERID)
                .commit();
        currentUser = null;
    }

    public void addFavProject(String project){
    	String serializedString = prefs.getString(PREFERENCE_FAV_PROJECT, null);
    	List<String> list=null;
    	if(serializedString == null) {
    		list = new ArrayList<String>();
    	} else {
    	    list = new ArrayList<String>(Arrays.asList(TextUtils.split(serializedString, ",")));
    	}  
    	
    	boolean isPresent = false;
    	if(list!=null)
    	{
    		for(String p:list)
    		{
    			if(p.equals(project))
    			{
    				AppConstants.log("Already present in the list! So there is no need to add");
    				isPresent = true;
    				break;
    			}
    		}
    		if(isPresent == false)
    		{
    			list.add(project);
    			prefs.edit().putString(PREFERENCE_FAV_PROJECT, TextUtils.join(",", list)).commit();
    			AppConstants.log("Added the project into the list");
    		}
    	}
    }
    
    public List<String> getFavProject()
    {
    	String serializedString = prefs.getString(PREFERENCE_FAV_PROJECT, null);
    	if (serializedString != null) 
    		return Arrays.asList(TextUtils.split(serializedString, ","));
    	else
    		return null;
    }
}
