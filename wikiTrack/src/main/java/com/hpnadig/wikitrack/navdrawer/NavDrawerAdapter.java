package com.hpnadig.wikitrack.navdrawer;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;


public class NavDrawerAdapter extends ArrayAdapter<DrawerItem> {

	Context context;
	List<DrawerItem> drawerItemList;
	int layoutResID;
	
	
	public NavDrawerAdapter(Context context,int layoutResourceID,List<DrawerItem> list)
	{
		super(context, layoutResourceID, list);
		this.context = context;
		this.drawerItemList = list;
		this.layoutResID = layoutResourceID;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
//		if(convertView==null)
//		{
//			convertView = ((Activity)context).getLayoutInflater().inflate(layoutResID, parent, false);
//		}
//
//		TextView tvTitle = (TextView)convertView.findViewById(R.id.drawer_itemName);
//		ImageView ivIcon = (ImageView)convertView.findViewById(R.id.drawer_icon);
//
//		DrawerItem drawerItem = drawerItemList.get(position);
//		if(drawerItem!=null)
//		{
//			tvTitle.setText(drawerItemList.get(position).getItemName());
//			ivIcon.setImageDrawable(context.getResources().getDrawable(drawerItem.getImgResID()));
//		}
		
		return convertView;
	}
	
}
