package com.hpnadig.wikitrack.navdrawer;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.hpnadig.wikitrack.AppConstants;
import com.hpnadig.wikitrack.PreDashboardActivity;
import com.hpnadig.wikitrack.R;
import com.hpnadig.wikitrack.WikiTrack;
import com.hpnadig.wikitrack.recentchanges.ActivityDisplayRecentChangesPages;
import com.hpnadig.wikitrack.usercontribution.UCInputActivity;
import com.hpnadig.wikitrack.watchlist.WatchListActivity;

import org.wikipedia.settings.SettingsActivity;

import java.util.List;

public class WikiBaseActivity extends ActionBarActivity {
	
	public DrawerLayout mDrawerLayout;
	public ListView mDrawerList;
	public ActionBarDrawerToggle mDrawerToggle;
	public List<DrawerItem> drawerItemList;
	public NavDrawerAdapter navDrawerAdapter;
	public ActionBar actionBar;
	private FragmentManager fragmentManager;
	private WikiTrack app;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    app =(WikiTrack) getApplication();
	    setContentView(R.layout.activity_base_wiki);
//	    actionBar = getSupportActionBar();
//	    actionBar.setDisplayHomeAsUpEnabled(true);
//		actionBar.setHomeButtonEnabled(true);
//
//	    // fragmentManager = getSupportFragmentManager();
//
//	    try {
//	    	ViewConfiguration config = ViewConfiguration.get(this);
//	    	Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
//	    	if (menuKeyField != null)
//	    	{
//	    		menuKeyField.setAccessible(true);
//	    		menuKeyField.setBoolean(config, false);
//	    	}
//	    } catch (Exception e) {
//	    	e.printStackTrace();
//	    }
	}
	
	/*public void setNavDrawer()
	{
		drawerItemList = new ArrayList<DrawerItem>();
	    actionBar = getSupportActionBar();
	    mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.left_drawer);
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
		
		drawerItemList.add(new DrawerItem("Login",R.drawable.ic_launcher));
		drawerItemList.add(new DrawerItem("Recent Changes",R.drawable.ic_launcher));
		drawerItemList.add(new DrawerItem("User Contributions",R.drawable.ic_launcher));
		drawerItemList.add(new DrawerItem("Watch List",R.drawable.ic_launcher));
	    
	    navDrawerAdapter = new NavDrawerAdapter(this, R.layout.layout_drawer_item,drawerItemList);
	    mDrawerList.setAdapter(navDrawerAdapter);
	    
	    actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setHomeButtonEnabled(true);	
		
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,R.drawable.ic_drawer, R.string.app_name,R.string.app_name) {
			public void onDrawerClosed(View view) {
				supportInvalidateOptionsMenu(); 
			}

			public void onDrawerOpened(View drawerView) {
			    supportInvalidateOptionsMenu();
			}
		};

		mDrawerLayout.setDrawerListener(mDrawerToggle);
	}
	*/
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.wiki_base, menu);
		return super.onCreateOptionsMenu(menu);
	}
/*	 
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggles
		mDrawerToggle.onConfigurationChanged(newConfig);
	}
	*/
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	/*	if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}*/
		
		switch (item.getItemId()) {
		    case android.R.id.home:
		    	onBackPressed();
		    	return true;
		    case R.id.action_fav_project:
		    	 String arr;
		    	 List<String> listFavProject = app.getUserInfoStorage().getFavProject();
		    	 if(listFavProject != null && listFavProject.size()>0) {
			    	 final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,listFavProject);
			    	 AlertDialog.Builder builder = new AlertDialog.Builder(this);
			    	 builder.setTitle(R.string.favourite_project)
			    	           .setAdapter(adapter, new DialogInterface.OnClickListener() {
			    	               public void onClick(DialogInterface dialog, int which) {
			    	            	   AppConstants.log("+++++ list item clicked ++++++");
			    	            	   // Since we'll have to store the project key, not the project name. 
			    	            	   String projectName = adapter.getItem(which);
			    	            	   String[] wikiProjectKeys = getResources().getStringArray(R.array.preference_project_keys);
			    	               	   String[] wikiProjectCanoName = getResources().getStringArray(R.array.preference_project_canonical_name);
			    	               	   for(int i=0;i<wikiProjectCanoName.length;i++)
				    	               {
				    	               	  if(projectName.equals(wikiProjectKeys[i]))
				    	               	  {
				    	               			app.getPreferences().edit().putString(AppConstants.WIKI_PROJECT,wikiProjectKeys[i]).commit();
				    	               			break;
				    	               	  }
				    	              }
			    	           }
			    	    });
			    	builder.create().show();  
		    	 } else {
		    		 AppConstants.toast(this.getApplicationContext(),getResources().getString(R.string.message_no_fav_projects));
		    	 }
				break;	
			case R.id.action_change_language_project:
				Intent chLanguage = new Intent(this,PreDashboardActivity.class);
				startActivity(chLanguage);
				break;
			case R.id.action_watchlist:
				Intent chProject = new Intent(this,WatchListActivity.class);
				startActivity(chProject);
				break;
			case R.id.action_user_contributions:
				Intent ucUserContribs = new Intent(this,UCInputActivity.class);
				startActivity(ucUserContribs);
				break;
			case R.id.action_recent_changes:
				Intent rcRecentChanges = new Intent(this,ActivityDisplayRecentChangesPages.class);
				startActivity(rcRecentChanges);
				break;
			case R.id.action_settings:
				Intent more = new Intent(this,SettingsActivity.class);
				startActivity(more);
				break;
			default:
				break;
		}
		return super.onOptionsItemSelected(item);
	}
	/* 
	private class DrawerItemClickListener implements ListView.OnItemClickListener
	{
		@Override
		public void onItemClick(AdapterView<?> listView, View item, int position,long id) {
			switch (position) {
				case 0:
					Intent chLanguage = new Intent(getApplicationContext(),LoginActivity.class);
					startActivity(chLanguage);
					break;
				case 1:
					Intent chProject = new Intent(getApplicationContext(),PreDashboardActivity.class);
					startActivity(chProject);
					break;
				case 2:
					Intent ucUserContribs = new Intent(this,UCInputActivity.class);
					startActivity(ucUserContribs);
					break;
				case 3:
					Intent rcRecentChanges = new Intent(this,ActivityDisplayRecentChangesPages.class);
					startActivity(rcRecentChanges);
					break;
				case 4:
					Intent more = new Intent(this,SettingsActivity.class);
					startActivity(more);
					break;
				default:
					break;
		}
		}
		
	}*/
	
}
