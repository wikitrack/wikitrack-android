package com.hpnadig.wikitrack.parser;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.hpnadig.wikitrack.AppConstants;
import com.hpnadig.wikitrack.recentchanges.RecentChanges;
import com.hpnadig.wikitrack.revision.Diff;
import com.hpnadig.wikitrack.revision.Revisions;
import com.hpnadig.wikitrack.usercontribution.UCItem;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 * Temporary parser. Later, I'll use the gson library to parse the json data. 
 * 
 * 
 **/

public class Parser {

        public static List<RecentChanges> parserRC(String rcJsonString)
        {
        	try {
        			
        			GsonBuilder builder = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        	    	builder.serializeNulls();
        			Gson gson = builder.create();
        			Type typeList = new TypeToken<List<RecentChanges>>(){}.getType();
        			List<RecentChanges> list= (List<RecentChanges>)gson.fromJson(rcJsonString,typeList);
        	    	return list;
				
				} catch (Exception e) {
					e.printStackTrace();
					AppConstants.log("Exception while parsing the data.");
				}
        	
        	return null;
        }
        
    
        //Just for temporary. Later we do the same in a better way
        public static Diff parserRV(JSONObject json,String pageID)
        {
        	try {
        			Diff diffObj = new Diff();
        			JSONObject	query =json.getJSONObject("query");
        			JSONObject recentchanges = query.getJSONObject("pages");
        			JSONObject pageId = recentchanges.getJSONObject(pageID);
        			JSONArray  revisions= pageId.getJSONArray("revisions");
        			JSONObject page = revisions.getJSONObject(0);
        			diffObj.setUser(page.getString("user"));
        			diffObj.setTime(page.getString("timestamp"));
        			
        			JSONObject diff = page.getJSONObject("diff");
        			String rev = diff.getString("*");
        			diffObj.setDiff(rev);
        			diffObj.setFrom(diff.getString("from"));
        			diffObj.setTo(diff.getString("to"));
        			
        			
        			return diffObj;
				} catch (Exception e) {
					e.printStackTrace();
					AppConstants.log("Exception while parsing the data.");
				}
        	
        	return null;
        }
	
        //Just for temporary. Later we do the same in a better way
        public static List<Revisions> parserRv(JSONObject json,String pageID)
        {
        	try {
        			List<Revisions> rvList =new ArrayList<Revisions>();
        			JSONObject	query =json.getJSONObject("query");
        			JSONObject recentchanges = query.getJSONObject("pages");
        			JSONObject pageId = recentchanges.getJSONObject(pageID);
        			JSONArray  revisions= pageId.getJSONArray("revisions");
        			for(int i=0; i<revisions.length(); i++)
        			{
        				Revisions rv = new Revisions();
        				JSONObject revision = revisions.getJSONObject(i);
        				String rev = revision.getString("*");
        				AppConstants.log("Revision "+i+ "\n" + rev);
        				rv.setRevision(rev);
        				rvList.add(rv);
        			}
        			return rvList;
				} catch (Exception e) {
					e.printStackTrace();
					AppConstants.log("Exception while parsing the data.");
				}
        	return null;
        }
        
        
        //Just for temporary. Later we do the same in a better way
        public static List<UCItem> parserUC(JSONArray usercontribs,String pageID)
        {
        	try {
        			List<UCItem> listUC = new ArrayList<UCItem>();
        			for(int i=0;i<usercontribs.length();i++)
        			{
        				JSONObject ucJson = usercontribs.getJSONObject(i);
        				UCItem item = new UCItem();
        				item.setTitle(ucJson.getString("title"));
        				item.setTimestamp(ucJson.getString("timestamp"));
        				item.setSize(ucJson.getString("size"));
        				item.setComment(ucJson.getString("comment"));
        				item.setPageid(ucJson.getString("pageid"));
        				item.setRevid(ucJson.getString("revid"));
        				listUC.add(item);
        			}
        			return listUC;
				} catch (Exception e) {
					e.printStackTrace();
					AppConstants.log("Exception while parsing the data.");
				}
        	
        	return null;
        }
}
