package com.hpnadig.wikitrack.recentchanges;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.hpnadig.wikitrack.AppConstants;
import com.hpnadig.wikitrack.R;
import com.hpnadig.wikitrack.Utils;
import com.hpnadig.wikitrack.WikiTrack;
import com.hpnadig.wikitrack.events.ProjectChangeEvent;
import com.hpnadig.wikitrack.parser.Parser;
import com.hpnadig.wikitrack.revision.ActivityRevision;
import com.hpnadig.wikitrack.user.UserDetailsActivity;
import com.squareup.otto.Subscribe;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
/**
 *
 * Retrieve the extra url from the previous activity.
 * Display the titles in a list Recent Changes articles
 *
 *
 **/
public class ActivityDisplayRecentChangesPages extends AppCompatActivity implements OnScrollListener, SwipeRefreshLayout.OnRefreshListener{

    private ExpandableListView listViewRC;
    private SwipeRefreshLayout refreshLayout;
    private TextView emptyView;
    private ProgressBar mProgressBar;
    private TextView footerView;
    private ListAdaptar adapter;
    private List<RecentChanges> entries = new ArrayList<>();

    boolean refresh = false;
    private int threshold = 2;
    WikiTrack app;
    boolean showLoading = false;
    private static String lastTimestamp;


    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onNewIntent(getIntent());
    }

    @SuppressLint("all")
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        app = (WikiTrack)getApplication();
        app.getBus().register(this);
        setContentView(R.layout.template_expandablelistview);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        AppConstants.log("In the Display Recent Changes activity");

        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar_list);
        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_layout);
        refreshLayout.setOnRefreshListener(this);
        emptyView = (TextView) findViewById(R.id.emptyView);
        footerView = (TextView) findViewById(R.id.footerView);
        listViewRC = (ExpandableListView) findViewById(R.id.listview);

        // set up the expendable list view
        adapter = new ListAdaptar(ActivityDisplayRecentChangesPages.this, R.layout.layout_card_ui_type_2, entries);
        listViewRC.setAdapter(adapter);
        listViewRC.setOnScrollListener(ActivityDisplayRecentChangesPages.this);
        setCanLoadMore(true);

        Display display = getWindowManager().getDefaultDisplay();
        int screenWidth = display.getWidth();

        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2){
            listViewRC.setIndicatorBounds(screenWidth-50, screenWidth);
        } else {
            listViewRC.setIndicatorBoundsRelative(screenWidth-50, screenWidth);
        }

        listViewRC.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                ListAdaptar adapter = (ListAdaptar) parent.getExpandableListAdapter();
                RecentChanges itemRC = (RecentChanges)adapter.getChild(groupPosition, childPosition);
                AppConstants.log("Clicked Item :: "+ itemRC);
                Intent startRevisionActivity = new Intent(ActivityDisplayRecentChangesPages.this,ActivityRevision.class);
                startRevisionActivity.putExtra(AppConstants.EXTRA_PAGEID, itemRC.getPageid());
                startRevisionActivity.putExtra(AppConstants.EXTRA_PAGETITLE, itemRC.getTitle());
                startRevisionActivity.putExtra(AppConstants.EXTRA_REVID, itemRC.getRevid());
                startActivity(startRevisionActivity);
                return true;
            }
        });
        makeServerCall();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void makeServerCall()
    {
        new RecentChangesApiTask(this, app.getPrimarySite(), lastTimestamp){
      @Override
            public void onCatch(Throwable caught) {
                setCanLoadMore(false);
                Toast.makeText(ActivityDisplayRecentChangesPages.this, caught.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFinish(RecentChangeResult result) {
                super.onFinish(result);
                if (result.getRcJsonData() == null) {
                    showMessage("Unable to retrieve data from the server.");
                }

                List<RecentChanges> entries = Parser.parserRC(result.getRcJsonData());
                if (entries == null) {
                    setCanLoadMore(false);
                }

                if (refresh && refreshLayout.isRefreshing()) {
                    adapter.clear();
                    refreshLayout.setRefreshing(false);
                    Toast.makeText(ActivityDisplayRecentChangesPages.this, "Refreshed Successfully.", Toast.LENGTH_SHORT).show();
                }

                // If the last timestamp is same, it means the dublicate data is coming.
//				if(lastTimestamp != null && lastTimestamp.equals(entries.get(entries.size() - 1).getTimestamp())) {
//					setCanLoadMore(false);
//					return;
//				}
//				lastTimestamp = entries.get(entries.size()-1).getTimestamp();

                adapter.addItems(entries);
                setCanLoadMore(true);

            }
        }.execute();
    }

    private void showMessage(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OKAY", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        builder.create().show();
    }

    /*
     * Show or Hide the footer view as well as the empty view.
     **/
    protected void setCanLoadMore(boolean mCanLoadMore) {
        if (!mCanLoadMore && adapter.getGroupCount() == 0) {
            //No items have been retrieved from the server.
            //because of the any reason. may be error has occured or server don't have items
            emptyView.setVisibility(View.VISIBLE);
            mProgressBar.setVisibility(View.INVISIBLE);
            footerView.setVisibility(View.GONE);
        } else if (!mCanLoadMore && adapter.getGroupCount() > 0) {
            //Unable to retrieve more items from the server.Looks like reached at the end.
            mProgressBar.setVisibility(View.INVISIBLE);
            footerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.INVISIBLE);
        } else if (mCanLoadMore && adapter.getGroupCount() == 0) {
            emptyView.setVisibility(View.GONE);
            mProgressBar.setVisibility(View.VISIBLE);
            footerView.setVisibility(View.GONE);
        } else {
            emptyView.setVisibility(View.GONE);
            mProgressBar.setVisibility(View.GONE);
            footerView.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Subscribe
    public void onProjectChanged(ProjectChangeEvent projectChangeEvent){
        AppConstants.log("project has changed : "+projectChangeEvent.getProjectKey());
        ((ListAdaptar)listViewRC.getExpandableListAdapter()).clear();
        makeServerCall();
    }


    @SuppressLint("NewApi")
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Display display = getWindowManager().getDefaultDisplay();
        int screenWidth = display.getWidth();
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2){
            listViewRC.setIndicatorBounds(screenWidth-50, screenWidth);
        } else {
            listViewRC.setIndicatorBoundsRelative(screenWidth-50, screenWidth);
        }
    }
    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,int visibleItemCount, int totalItemCount) {
        //No need
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        if (scrollState == SCROLL_STATE_IDLE) {
            AppConstants.log("SCROLL_STATE_IDLE");
            if (listViewRC.getLastVisiblePosition() >= listViewRC.getCount() - 1 - threshold) {
                AppConstants.log("load more items");
                footerView.setVisibility(View.VISIBLE);
                makeServerCall();
            }
        }
    }

    @Override
    public void onRefresh() {
        AppConstants.log("On refresh started");
        refresh = true;
        makeServerCall();
    }

    /**
     *
     * Adapter for showing the titles of the Recent Change pages.
     * Also, it downloads the data and fills the data into the list view declared above.
     *
     **/


    /**
     * ExpandableListAdapeter shows the titles as the header and
     * RecentChanges Items as the child. I have used Map for the mapping
     * between the String title and RecentChange Items.
     * */
    class ListAdaptar extends BaseExpandableListAdapter
    {
        private Context context;
        private int layoutResource;
        List<RecentChanges> entries;
        ArrayList<String> listHeaderTitles = new ArrayList<String>();

        Map<String, List<RecentChanges>> rcCollection = new LinkedHashMap<String, List<RecentChanges>>();;

        public
        ListAdaptar(Context context, int resource, List<RecentChanges> entries) {
            this.context=context;
            this.layoutResource=resource;
            this.entries = entries;
        }

        /**
         * Split the list into header titles and Recent Changes items.
         *
         * */
        private void splitList(List<RecentChanges> entries) {

            Map<String, List<RecentChanges>> rcCollectionTemp = new LinkedHashMap<String, List<RecentChanges>>();;
            // iterate through your objects
            for(RecentChanges rcItem : entries){
                // fetch the list for this object's id
                List<RecentChanges> temp = rcCollectionTemp.get(rcItem.getTitle());

                if(temp == null){
                    // if the list is null we haven't seen an
                    // object with this id before, so create
                    // a new list
                    temp = new ArrayList<RecentChanges>();

                    // and add it to the map
                    rcCollectionTemp.put(rcItem.getTitle(), temp);
                }

                // whether we got the list from the map
                // or made a new one we need to add our
                // object.
                temp.add(rcItem);
            }
            listHeaderTitles.addAll(rcCollectionTemp.keySet());
            rcCollection.putAll(rcCollectionTemp);
        }

        public void clear()
        {
            listHeaderTitles.clear();
            rcCollection.clear();
        }

        public void addItems(List<RecentChanges> newItems) {
            entries.addAll(newItems);
            splitList(entries);
            this.notifyDataSetChanged();
        }

        @Override
        public int getGroupCount() {
            //return rcCollection.keySet().size();
            return listHeaderTitles.size();
        }

        //TODO Needs to write the conditional logic here.
        @Override
        public int getChildrenCount(int groupPosition) {
            //List<RecentChanges> list = (List<RecentChanges>)new ArrayList(rcCollection.values()).get(groupPosition);
            return rcCollection.get(listHeaderTitles.get(groupPosition)).size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return new ArrayList(rcCollection.values()).get(groupPosition);
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return rcCollection.get(listHeaderTitles.get(groupPosition)).get(childPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return getChild(groupPosition,childPosition).hashCode();
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded,View convertView, ViewGroup parent) {
            if(convertView == null)
            {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.layout_single_textview,parent,false);
            }
            TextView tvTitleHeader = (TextView)convertView.findViewById(R.id.textViewId);
            TextView tvTimestamp = (TextView)convertView.findViewById(R.id.tv_timestamp);
            String title = listHeaderTitles.get(groupPosition);
            tvTitleHeader.setText(title);
            Date timestamp = ((List<RecentChanges>)rcCollection.get(title)).get(0).getTimestamp();
            if (timestamp != null) {
                SimpleDateFormat sdf = new SimpleDateFormat(AppConstants.dateFormat);
                String dateString = sdf.format(timestamp);
                tvTimestamp.setText(dateString);
            }

            return convertView;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition,boolean isLastChild, View convertView, ViewGroup parent) {
            if(convertView == null)
            {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(layoutResource, parent, false);
            }
            Utils.setTextDirection(convertView, app.getPrimaryLanguage());

            final RecentChanges RCItem = (RecentChanges)getChild(groupPosition, childPosition);
            if(RCItem !=null)
            {
                TextView textViewTitle = (TextView)convertView.findViewById(R.id.tv_title);
                TextView textViewDate = (TextView)convertView.findViewById(R.id.tv_date);
                TextView textViewComment = (TextView)convertView.findViewById(R.id.tv_comment);
                TextView textViewUser = (TextView)convertView.findViewById(R.id.tv_user);


                textViewUser.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intentUserDetail = new Intent(ActivityDisplayRecentChangesPages.this, UserDetailsActivity.class);
                        intentUserDetail.putExtra(UserDetailsActivity.ARGS_USERNAME, RCItem.getUser());
                        startActivity(intentUserDetail);
                    }
                });

                AppConstants.log("Item: " + RCItem.getTitle());
                textViewTitle.setText(RCItem.getTitle());


                if (RCItem.getTimestamp() != null) {
                    SimpleDateFormat sdf = new SimpleDateFormat(AppConstants.dateFormat);
                    String dateString = sdf.format(RCItem.getTimestamp());
                    textViewDate.setText(dateString);
                }


                String comment  = ( RCItem.getComment() == null || RCItem.getComment().length() == 0 ) ? "No Comments Found": RCItem.getComment();
                textViewComment.setText(comment);
                textViewUser.setText(RCItem.getUser());

                return convertView;
            }
            return null;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

    }


}
