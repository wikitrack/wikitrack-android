package com.hpnadig.wikitrack.recentchanges;


public class RecentChangeResult {
	
	private String rcJsonData;
	
	public RecentChangeResult(String jsonData) {
		this.rcJsonData = jsonData;
	}
	
	public String getRcJsonData() {
		return rcJsonData;
	}
}
