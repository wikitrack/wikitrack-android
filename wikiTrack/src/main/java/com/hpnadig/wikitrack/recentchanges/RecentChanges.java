package com.hpnadig.wikitrack.recentchanges;

import java.io.Serializable;
import java.util.Date;

public class RecentChanges implements Serializable{

	 	private String title;

	    private String ns;

	    private String revid;

	    private String old_revid;

	    private String type;

	    private String rcid;

	    private String pageid;

	    private String user;
	    private String comment;
	    private Date timestamp;
	    private String flag;
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public String getNs() {
			return ns;
		}
		public void setNs(String ns) {
			this.ns = ns;
		}
		public String getRevid() {
			return revid;
		}
		public void setRevid(String revid) {
			this.revid = revid;
		}
		public String getOld_revid() {
			return old_revid;
		}
		public void setOld_revid(String old_revid) {
			this.old_revid = old_revid;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public String getRcid() {
			return rcid;
		}
		public void setRcid(String rcid) {
			this.rcid = rcid;
		}
		public String getPageid() {
			return pageid;
		}
		public void setPageid(String pageid) {
			this.pageid = pageid;
		}
		public String getUser() {
			return user;
		}
		public void setUser(String user) {
			this.user = user;
		}
		public String getComment() {
			return comment;
		}
		public void setComment(String comment) {
			this.comment = comment;
		}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public String getFlag() {
			return flag;
		}
		public void setFlag(String flag) {
			this.flag = flag;
		}
	    
	    
	    
}
