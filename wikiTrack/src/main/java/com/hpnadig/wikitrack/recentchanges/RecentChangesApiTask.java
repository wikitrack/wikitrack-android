package com.hpnadig.wikitrack.recentchanges;

import android.content.Context;

import com.hpnadig.wikitrack.AppConstants;
import com.hpnadig.wikitrack.Site;
import com.hpnadig.wikitrack.WikiTrack;

import org.json.JSONArray;
import org.mediawiki.api.json.Api;
import org.mediawiki.api.json.ApiResult;
import org.mediawiki.api.json.RequestBuilder;
import org.wikipedia.concurrency.SaneAsyncTask;

public class RecentChangesApiTask extends SaneAsyncTask<RecentChangeResult>{

	private WikiTrack app;
	private Api api;
	private String rcStart;
	
	public RecentChangesApiTask(Context context,Site site, String rcStart) {
		super(SINGLE_THREAD);
		app = (WikiTrack)context.getApplicationContext();
		this.rcStart = rcStart;
		api = app.getAPIForSite(site);
	}
	
	@Override
	public RecentChangeResult performTask() throws Throwable {
		RequestBuilder rcRequestBuilder = api.action("query")
							  .param("list","recentchanges")
							  .param("format","json")
							  .param("rcdir", "newer")
							  .param("rcnamespace", "0")
							  .param("rcprop", "title|ids|user|comment|timestamp|flag")
							  .param("rclimit", "10")
							  .param("rctype", "edit");
		if (rcStart != null)
			rcRequestBuilder.param("rcStart", rcStart);

		ApiResult result = rcRequestBuilder.get();
		AppConstants.log("result :: "+result.asObject().toString());
		JSONArray jsonRClist = result.asObject().optJSONObject("query").optJSONArray("recentchanges");
		return new RecentChangeResult(jsonRClist.toString());
	}
	
	@Override
	public void onFinish(RecentChangeResult result) {
		super.onFinish(result);
	}

}
