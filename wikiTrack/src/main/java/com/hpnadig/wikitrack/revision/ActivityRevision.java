package com.hpnadig.wikitrack.revision;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.view.MenuItem;
import android.webkit.WebView;

import com.hpnadig.wikitrack.AppConstants;
import com.hpnadig.wikitrack.R;
import com.hpnadig.wikitrack.WikiTrack;
import com.hpnadig.wikitrack.parser.Parser;
import com.squareup.otto.Subscribe;

import java.util.LinkedList;
import java.util.List;

import info.bliki.wiki.filter.PlainTextConverter;
import info.bliki.wiki.model.WikiModel;


/**
 * 
 *    Activity, which shows the revisions in a webview, specifically <table> tag of HTML.
 * 
 *    extraRevID      For getting the html diffs to the compare to previous revision
 *    extraRVPageID   For parsing the json response from the server.
 * 
 * */

public class ActivityRevision extends AppCompatActivity {

	String extraRVPageID,extraRevID;
	ProgressDialog mProgressDialog;
	WikiTrack app;
	WebView webViewRVDiff;
	String tvTitle,tvUser,tvTime;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		app = (WikiTrack)getApplication();
		app.getBus().register(this);
		AppConstants.log("Inside the Activity Revision");
		setContentView(R.layout.activity_revision1);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		webViewRVDiff = (WebView) findViewById(R.id.webView_rv_diffs);		
		extraRVPageID = getIntent().getStringExtra(AppConstants.EXTRA_PAGEID);
		extraRevID = getIntent().getStringExtra(AppConstants.EXTRA_REVID);
		AppConstants.log("page id :: "+extraRVPageID);
		
		//Need to change it later
		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setMessage("Please wait...\nThis will take few moments.");
		mProgressDialog.show();
	}

	@Override
	protected void onResume() {
		super.onResume();
		if(app.isOnline())
		{
			showDiff(extraRevID);
		} else {
			AppConstants.toast(this,getString(R.string.message_check_internet));
		}
	}
	
	private List<Revisions> revs;
	@Subscribe
	public void onResponse(DiffFetchResult jsonData) {
		try {
				AppConstants.log("gotten the json data :: "+jsonData.getDiff());
				revs = Parser.parserRv(jsonData.getDiff(),extraRVPageID);
				mProgressDialog.dismiss();
				
				if(revs.size() == 2) {
					String revisionXWikiCurrent = revs.get(0).getRevision();
					String revisionXWikiNext = revs.get(1).getRevision();
					
					//convert that into html
					WikiModel wikiModel = new WikiModel("http://en.wikipedia.org/wiki/${image}", "http://en.wikipedia.org/wiki/${title}");
					String revisionTextCurrent = wikiModel.render(new PlainTextConverter(),revisionXWikiCurrent);
					String revisionTextNext = wikiModel.render(new PlainTextConverter(),revisionXWikiNext);
					   	   
					//Testing the diffs in html
					diff_match_patch dmp = new diff_match_patch();
					LinkedList<com.hpnadig.wikitrack.revision.diff_match_patch.Diff> list = dmp.diff_main(revisionTextCurrent, revisionTextNext);
					dmp.diff_cleanupSemantic(list);
					String _diff_html = dmp.diff_prettyHtml(list);
					String diff_html = _diff_html.replaceAll("&para;","");
					AppConstants.log("Coverted Html ::"+diff_html);
					   
					//load the diffs  
					webViewRVDiff.loadDataWithBaseURL(null,diff_html,"text/html","UTF-8",null);
				} else {
					AppConstants.toast(this,getString(R.string.message_single_revision));
					String revisionXWikiCurrent = revs.get(0).getRevision();
					WikiModel wikiModel = new WikiModel("http://en.wikipedia.org/wiki/${image}", "http://en.wikipedia.org/wiki/${title}");
					String revisionTextCurrent = wikiModel.render(new PlainTextConverter(),revisionXWikiCurrent);
					webViewRVDiff.loadDataWithBaseURL(null,new SpannableString(revisionTextCurrent).toString(),"text/html","UTF-8",null);
				}
				
		} catch (Exception e) {
			    e.printStackTrace();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			this.finish();
		}
		return super.onOptionsItemSelected(item);
	}


	private void showDiff(String revId)
	{
		new DiffFetchTask(this,app.getPrimarySite(),extraRVPageID).execute();
	}
}
