package com.hpnadig.wikitrack.revision;

import org.json.JSONObject;

public class DiffFetchResult {

	private JSONObject diff;
	
	public DiffFetchResult(JSONObject diff) {
		this.diff=diff;
	}
	
	public JSONObject getDiff() {
		return diff;
	}
}
