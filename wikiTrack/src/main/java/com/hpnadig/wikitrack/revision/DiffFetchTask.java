package com.hpnadig.wikitrack.revision;

import org.json.JSONObject;
import org.mediawiki.api.json.Api;
import org.mediawiki.api.json.ApiResult;
import org.wikipedia.concurrency.SaneAsyncTask;
import android.content.Context;
import com.hpnadig.wikitrack.AppConstants;
import com.hpnadig.wikitrack.Site;
import com.hpnadig.wikitrack.WikiTrack;

public class DiffFetchTask extends SaneAsyncTask<DiffFetchResult>{

	private WikiTrack app;
	private Api api;
	private String pageId;
	
	public DiffFetchTask(Context context,Site site,String pageId) {
		super(SINGLE_THREAD);
		app = (WikiTrack)context.getApplicationContext();
		api = app.getAPIForSite(site);
		this.pageId=pageId;
	}
	
	@Override
	public DiffFetchResult performTask() throws Throwable {
		ApiResult result = api.action("query")
							  .param("prop","revisions")
							  .param("rvlimit","2")
							  .param("format", "json")
							  .param("rvprop", "content|timestamp|user|comment")
							  .param("rvdir","newer")
							  .param("rvcontentformat","text/x-wiki")
							  .param("pageids",pageId)
							  .get();
		AppConstants.log("result :: "+result.asObject().toString());
		JSONObject jsonRClist = result.asObject();
		return new DiffFetchResult(jsonRClist);
	}
	
	@Override
	public void onFinish(DiffFetchResult result) {
		super.onFinish(result);
		app.getBus().post(result);
	}

}
