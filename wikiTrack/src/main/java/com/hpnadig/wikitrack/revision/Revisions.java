package com.hpnadig.wikitrack.revision;

public class Revisions {
	private String contentformat;

    private String contentmodel;

    private String revision;

    public String getContentformat ()
    {
        return contentformat;
    }

    public void setContentformat (String contentformat)
    {
        this.contentformat = contentformat;
    }

    public String getContentmodel ()
    {
        return contentmodel;
    }

    public void setContentmodel (String contentmodel)
    {
        this.contentmodel = contentmodel;
    }

    public String getRevision() {
		return revision;
	}
    
    public void setRevision(String revision) {
		this.revision = revision;
	}

	@Override
	public String toString() {
		return "Revisions [contentformat=" + contentformat + ", contentmodel="
				+ contentmodel + ", revision=" + revision + "]";
	}
    
    
}
