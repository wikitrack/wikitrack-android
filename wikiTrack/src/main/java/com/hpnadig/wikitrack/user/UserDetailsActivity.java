package com.hpnadig.wikitrack.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.hpnadig.wikitrack.AppConstants;
import com.hpnadig.wikitrack.R;
import com.hpnadig.wikitrack.WikiTrack;
import com.hpnadig.wikitrack.revision.ActivityRevision;
import com.hpnadig.wikitrack.usercontribution.OnUserContributionClickListener;
import com.hpnadig.wikitrack.usercontribution.UCItem;
import com.hpnadig.wikitrack.usercontribution.UserContributionFragment;

import org.json.JSONObject;

public class UserDetailsActivity extends AppCompatActivity implements OnUserContributionClickListener {

    private String username;
    public static final String ARGS_USERNAME = "args_username";
    private WikiTrack app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        app = (WikiTrack) getApplication();

        username = getIntent().getStringExtra(ARGS_USERNAME);
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);

        if (username != null) {
            new UserDetailsRetrieveTask(getApplicationContext(), app.getPrimarySite(), username) {

                @Override
                public void onBeforeExecute() {
                    super.onBeforeExecute();
                    progressBar.setVisibility(View.VISIBLE);
                }

                @Override
                public void onFinish(UserDetailsResult result) {
                    super.onFinish(result);
                    if (result.getResult().equalsIgnoreCase("Success")) {
                        findViewById(R.id.usercard).setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        displayResult(result.getUserDetails());
                        String username = result.getUserDetails().optString("name");
                        if (username == null) {
                            Toast.makeText(UserDetailsActivity.this, "Some error has occurred", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container,
                                UserContributionFragment.newInstance(username))
                                .commit();
                    }
                }

                @Override
                public void onCatch(Throwable caught) {
                    progressBar.setVisibility(View.GONE);
                }
            }.execute();
        } else {
            // todo handle this
            Toast.makeText(UserDetailsActivity.this, "User id is null", Toast.LENGTH_SHORT).show();
        }
    }

    private void displayResult(JSONObject jsonUserDetail) {

        TextView tvuserid = (TextView) findViewById(R.id.tv_userid);
        TextView tvusername = (TextView) findViewById(R.id.tv_username);
        TextView tveditcount = (TextView) findViewById(R.id.tv_editcount);

        String userid = jsonUserDetail.optString("userid");
        String username = jsonUserDetail.optString("name");
        String editCount = jsonUserDetail.optString("editcount");

        tvusername.setText(username);
        if (userid != null && !userid.isEmpty()) {
            tvuserid.setText(userid);
        } else {
            ((TextView)findViewById(R.id.tv_name_id)).setText("Name");
        }
        if (editCount != null && !editCount.isEmpty()) {
            tveditcount.setText(editCount);
        } else {
            findViewById(R.id.block_editcount).setVisibility(View.GONE);
        }

    }

    @Override
    public void onUserContributionClick(UCItem ucItem) {
        Intent startRevisionActivity = new Intent(this, ActivityRevision.class);
        startRevisionActivity.putExtra(AppConstants.EXTRA_PAGEID, ucItem.getPageid());
        startRevisionActivity.putExtra(AppConstants.EXTRA_REVID, ucItem.getRevid());
        startActivity(startRevisionActivity);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
