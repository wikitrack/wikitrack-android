package com.hpnadig.wikitrack.user;

import org.json.JSONObject;

/**
 * Created by apple on 11/26/15.
 */
public class UserDetailsResult {
    private JSONObject userDetails;
    private String result;

    public UserDetailsResult(String result, JSONObject userDetails) {
        this.userDetails = userDetails;
        this.result = result;
    }

    public JSONObject getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(JSONObject userDetails) {
        this.userDetails = userDetails;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
