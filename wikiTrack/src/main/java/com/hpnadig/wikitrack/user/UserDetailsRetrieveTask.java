package com.hpnadig.wikitrack.user;

import android.content.Context;

import com.hpnadig.wikitrack.AppConstants;
import com.hpnadig.wikitrack.Site;
import com.hpnadig.wikitrack.WikiTrack;

import org.json.JSONObject;
import org.mediawiki.api.json.Api;
import org.mediawiki.api.json.ApiResult;
import org.wikipedia.concurrency.SaneAsyncTask;

/**
 * Created by apple on 11/26/15.
 */
public class UserDetailsRetrieveTask extends SaneAsyncTask<UserDetailsResult> {

    private WikiTrack app;
    private Api api;
    private String username;

    public UserDetailsRetrieveTask(Context context,Site site,String username) {
        super(SINGLE_THREAD);
        app = (WikiTrack)context.getApplicationContext();
        api = app.getAPIForSite(site);
        this.username =username;
    }

    @Override
    public UserDetailsResult performTask() throws Throwable {
        ApiResult apiResult = api.action("query").
                param("list","users")
                .param("format", "json")
                .param("usprop","blockinfo|groups|implicitgroups|rights|editcount|registration|emailable|gender")
                .param("ususers", username)
                .get();

        AppConstants.log("Json Data "+ apiResult.asObject().toString());
        JSONObject jsonUserDetail = apiResult.asObject().optJSONObject("query").optJSONArray("users").getJSONObject(0);

        UserDetailsResult result;
        if (jsonUserDetail != null) {
            return new UserDetailsResult("Success", jsonUserDetail);
        }


        return null;
    }
}
