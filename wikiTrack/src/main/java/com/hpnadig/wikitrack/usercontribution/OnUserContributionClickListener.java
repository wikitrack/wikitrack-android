package com.hpnadig.wikitrack.usercontribution;

/**
 * Created by poliveira on 27/10/2014.
 */
public interface OnUserContributionClickListener {
    void onUserContributionClick(UCItem item);
}
