package com.hpnadig.wikitrack.usercontribution;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.hpnadig.wikitrack.AppConstants;
import com.hpnadig.wikitrack.NonEmptyValidator;
import com.hpnadig.wikitrack.R;
import com.hpnadig.wikitrack.WikiTrack;

public class UCInputActivity extends AppCompatActivity implements OnClickListener{

	TextView tvUsername;
	EditText etUsernameInput;
	Button btnLoad;
	String username;
	NonEmptyValidator nonEmptyValidator;
	private WikiTrack app;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_uc_input_username);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		app =(WikiTrack)getApplication();
		etUsernameInput = (EditText) findViewById(R.id.edittext_username);
		btnLoad = (Button) findViewById(R.id.btnLoad);
		btnLoad.setOnClickListener(this);
		nonEmptyValidator = new NonEmptyValidator(new NonEmptyValidator.ValidationChangedCallback() {
			@Override
			public void onValidationChanged(boolean isValid) {
				btnLoad.setEnabled(isValid);
			}
		}, etUsernameInput);
		
				
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			this.finish();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		if(app.isOnline())
		{
			username = etUsernameInput.getText().toString();
			AppConstants.log("UC username  :: "+username);
			Intent showUC = new Intent(this, UserContributionActivity.class);
			showUC.putExtra(AppConstants.EXTRA_USER,username);
			startActivity(showUC);
		}else {
			AppConstants.toast(this,getString(R.string.message_check_internet));
		}
		
	}

	

}
