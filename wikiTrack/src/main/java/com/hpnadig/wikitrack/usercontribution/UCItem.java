package com.hpnadig.wikitrack.usercontribution;

public class UCItem {

	private String timestamp;

    private String ns;

    private String title;

    private String minor;

    private String userid;

    private String parentid;

    private String revid;

    private String comment;

    private String user;

    private String pageid;

    private String size;

    public String getTimestamp ()
    {
        return timestamp;
    }

    public void setTimestamp (String timestamp)
    {
        this.timestamp = timestamp;
    }

    public String getNs ()
    {
        return ns;
    }

    public void setNs (String ns)
    {
        this.ns = ns;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

  
    public String getMinor ()
    {
        return minor;
    }

    public void setMinor (String minor)
    {
        this.minor = minor;
    }

    public String getUserid ()
    {
        return userid;
    }

    public void setUserid (String userid)
    {
        this.userid = userid;
    }

    public String getParentid ()
    {
        return parentid;
    }

    public void setParentid (String parentid)
    {
        this.parentid = parentid;
    }

    public String getRevid ()
    {
        return revid;
    }

    public void setRevid (String revid)
    {
        this.revid = revid;
    }

    public String getComment ()
    {
        return comment;
    }

    public void setComment (String comment)
    {
        this.comment = comment;
    }

    public String getUser ()
    {
        return user;
    }

    public void setUser (String user)
    {
        this.user = user;
    }

    public String getPageid ()
    {
        return pageid;
    }

    public void setPageid (String pageid)
    {
        this.pageid = pageid;
    }

    public String getSize ()
    {
        return size;
    }

    public void setSize (String size)
    {
        this.size = size;
    }

	@Override
	public String toString() {
		return "UCItem [timestamp=" + timestamp + ", ns=" + ns + ", title="
				+ title + ", minor=" + minor + ", userid=" + userid
				+ ", parentid=" + parentid + ", revid=" + revid + ", comment="
				+ comment + ", user=" + user + ", pageid=" + pageid + ", size="
				+ size + "]";
	}
    
}
