package com.hpnadig.wikitrack.usercontribution;

public class UserConributionResult {

	private String ucJsonData;
	
	public UserConributionResult(String ucJsonData) {
		this.ucJsonData =ucJsonData; 

	}
	
	public String getUcJsonData() {
		return ucJsonData;
	}
}
