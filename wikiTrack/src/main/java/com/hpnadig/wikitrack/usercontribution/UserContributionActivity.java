package com.hpnadig.wikitrack.usercontribution;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.hpnadig.wikitrack.AppConstants;
import com.hpnadig.wikitrack.R;
import com.hpnadig.wikitrack.revision.ActivityRevision;

public class UserContributionActivity extends AppCompatActivity implements OnUserContributionClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_container);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String username = getIntent().getStringExtra(AppConstants.EXTRA_USER);
        if (username == null) {
            throw new RuntimeException("Username is null");
        }


        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, UserContributionFragment.newInstance(username))
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onUserContributionClick(UCItem ucItem) {
        Intent startRevisionActivity = new Intent(this, ActivityRevision.class);
        startRevisionActivity.putExtra(AppConstants.EXTRA_PAGEID, ucItem.getPageid());
        startRevisionActivity.putExtra(AppConstants.EXTRA_REVID, ucItem.getRevid());
        startActivity(startRevisionActivity);
    }
}
