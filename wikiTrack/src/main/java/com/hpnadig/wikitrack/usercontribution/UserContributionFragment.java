package com.hpnadig.wikitrack.usercontribution;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.hpnadig.wikitrack.AppConstants;
import com.hpnadig.wikitrack.R;
import com.hpnadig.wikitrack.WikiTrack;
import com.hpnadig.wikitrack.parser.Parser;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;


public class UserContributionFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private SwipeRefreshLayout refreshLayout;
    private RecyclerView recyclerView;
    private TextView emptyView;
    private ProgressBar progressBar;
    private TextView footerView;
    private Activity activity;
    private LinearLayoutManager layoutManager;
    private String TAG = "debug";
    private UserContributionAdapter adapter;

    //For pagination
    private boolean loading = true;
    private int previousTotal = 0;
    private int visibleThreshold = 2;
    private static int pageCount = 0;
    private static boolean isRefreshed = false;

    //For passing data between activity and this fragment
    private static final String ARGS_USERNAME = "args_username";
    private String username;
    private OnUserContributionClickListener mClickCallback;
    private String timeStamp;
    private WikiTrack app;
    private String msg_empty_view  = null;


    public static UserContributionFragment newInstance(String username) {
        UserContributionFragment f = new UserContributionFragment();
        Bundle b = new Bundle();
        b.putString(ARGS_USERNAME, username);
        f.setArguments(b);
        return f;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mClickCallback = (OnUserContributionClickListener) activity;
        } catch (ClassCastException e) {
            throw new RuntimeException(activity.getLocalClassName() + " must implement Callback interface");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "on create");
        activity = getActivity();
        app = (WikiTrack) getActivity().getApplication();
        username = getArguments().getString(ARGS_USERNAME);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_recycler_view, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refresh_layout);
        refreshLayout.setOnRefreshListener(this);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        emptyView = (TextView) view.findViewById(R.id.emptyView);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        footerView = (TextView) view.findViewById(R.id.footerView);

        //Set the layout manager
        layoutManager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(layoutManager);

        //Set the Adapter
        adapter = new UserContributionAdapter(activity);
        adapter.setItemClickCallback((OnUserContributionClickListener) activity);
        recyclerView.setAdapter(adapter);

        //Set the Scroll listener for the Pagination
        recyclerView.addOnScrollListener(new RecyclerViewScrollListener());

        setCanLoadMore(true);
        retrieveItems();
    }


    // Retrieves data from the server
    private void retrieveItems() {
        Log.d(TAG, "Going to make request");
        setCanLoadMore(true);
        new UserContributionTask(getActivity(), app.getPrimarySite(), username, timeStamp){
            @Override
            public void onFinish(UserConributionResult result) {
                super.onFinish(result);
                if (result == null || getActivity() == null) {
                    setCanLoadMore(false);
                    return;
                }
                try {
                    if (result.getUcJsonData().equals("[]")) {
                        String selectedProject = app.getWikiProject();
                        int langIndex = app.findWikiIndex(app.getPrimaryLanguage());
                        String selectedLanguage = app.localNameFor(langIndex);
                        msg_empty_view = "User <i>"+ username +"</i> has not contributed for project <i>"+ selectedProject+"</i> and language <i>"+ selectedLanguage+"</i>";
                        setCanLoadMore(false);
                        return;
                    }

                    List<UCItem> list = Parser.parserUC(new JSONArray(result.getUcJsonData()), null);
                    if (list == null) {
                        msg_empty_view = getString(R.string.message_no_more_items_found);
                        setCanLoadMore(false);
                        return;
                    }

                    if (list != null && list.size() > 0 ) {
                        if (isRefreshed && refreshLayout.isRefreshing()) {
                            isRefreshed = false;
                            refreshLayout.setRefreshing(false);
                            adapter.clear();
                            Toast.makeText(getActivity(), "Refresh Successfully", Toast.LENGTH_SHORT).show();
                        }

                        // Check for dublicate
                        if(timeStamp != null && timeStamp.equals(list.get(list.size()-1).getTimestamp())) {
                            AppConstants.log("Now going to repeat. So let it stop here");
                            setCanLoadMore(false);
                        }
                        timeStamp = list.get(list.size()-1).getTimestamp();

                        adapter.addItem(list);
                        setCanLoadMore(true);
                    }
                } catch (JSONException e) {
                    AppConstants.logE("Error while parsing ucitems", e);
                    setCanLoadMore(false);
                }
            }
        }.execute();
    }

    private void handleError(final Exception e) {
        Toast.makeText(activity, e.getMessage(), Toast.LENGTH_SHORT).show();
    }

    // Scroll listener for the Recylcer view
    class RecyclerViewScrollListener extends RecyclerView.OnScrollListener {

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            Log.d("RRF", "onScrolled");

            refreshLayout.setEnabled(layoutManager.findFirstVisibleItemPosition() == 0);

            int totalItemCount = layoutManager.getItemCount();
            int visibleItemCount = recyclerView.getChildCount();
            int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

            if (loading) {
                if (totalItemCount > previousTotal) {
                    loading = false;
                    previousTotal = totalItemCount;
                }
            }
            if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItemPosition + visibleThreshold)) {
                ++pageCount;
                loading = true;
                footerView.setVisibility(View.VISIBLE);
                retrieveItems();
            }
        }

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }
    }

    /*
     * Show or Hide the footer view as well as the empty view.
     **/
    protected void setCanLoadMore(boolean mCanLoadMore) {
        Log.d(TAG, "setCanLoadMore " + mCanLoadMore);
        if (!mCanLoadMore && adapter.getItemCount() == 0) {
            //No items have been retrieved from the server.
            //because of the any reason. may be error has occured or server don't have items
            emptyView.setText(Html.fromHtml(msg_empty_view));
            emptyView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.INVISIBLE);
            footerView.setVisibility(View.GONE);
        } else if (!mCanLoadMore && adapter.getItemCount() > 0) {
            //Unable to retrieve more items from the server.Looks like reached at the end.
            progressBar.setVisibility(View.INVISIBLE);
            footerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.INVISIBLE);
        } else if (mCanLoadMore && adapter.getItemCount() == 0) {
            emptyView.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            footerView.setVisibility(View.GONE);
        } else {
            emptyView.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            footerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRefresh() {
        isRefreshed = true;
        retrieveItems();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mClickCallback = null;
    }
}
