package com.hpnadig.wikitrack.usercontribution;

import android.content.Context;

import com.hpnadig.wikitrack.AppConstants;
import com.hpnadig.wikitrack.Site;
import com.hpnadig.wikitrack.WikiTrack;

import org.json.JSONArray;
import org.mediawiki.api.json.Api;
import org.mediawiki.api.json.ApiResult;
import org.mediawiki.api.json.RequestBuilder;
import org.wikipedia.concurrency.SaneAsyncTask;

public class UserContributionTask  extends SaneAsyncTask<UserConributionResult>{

	private WikiTrack app;
	private Api api;
	private String username;
	private String timeStamp;
	
	
	public UserContributionTask(Context context,Site site,String username,String timestamp) {
		super(SINGLE_THREAD);
		app = (WikiTrack)context.getApplicationContext();
		api = app.getAPIForSite(site);
		this.username =username;
		this.timeStamp = timestamp;
	}
	public UserContributionTask(Context context,Site site,String username) {
		super(SINGLE_THREAD);
		app = (WikiTrack)context.getApplicationContext();
		api = app.getAPIForSite(site);
		this.username =username;
	}
	
	@Override
	public UserConributionResult performTask() throws Throwable {
		RequestBuilder rb = api.action("query")
							  .param("list","usercontribs")
							  .param("format","json")
							  .param("ucdir", "older")
							  .param("ucuser",username);
		if(timeStamp!=null)
			rb.param("ucstart",timeStamp);
		
		ApiResult result = rb.get();
							  
		AppConstants.log("result :: "+result.asObject().toString());
		JSONArray jsonUClist = result.asObject().optJSONObject("query").optJSONArray("usercontribs");
		return new UserConributionResult(jsonUClist.toString());
	}
	
	@Override
	public void onFinish(UserConributionResult result) {
		super.onFinish(result);
	}

}
