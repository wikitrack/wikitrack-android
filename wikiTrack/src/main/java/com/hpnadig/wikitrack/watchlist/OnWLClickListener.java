package com.hpnadig.wikitrack.watchlist;

/**
 * Created by poliveira on 27/10/2014.
 */
public interface OnWLClickListener {
    void onNewsItemItemSelected(WLItem item);
}
