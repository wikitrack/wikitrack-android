package com.hpnadig.wikitrack.watchlist;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hpnadig.wikitrack.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rahul on 23/3/15.
 */
public class WLAdapter extends RecyclerView.Adapter<WLAdapter.Item> {

    Activity mActivity;
    protected List<WLItem> mItems = new ArrayList<>();
    private OnWLClickListener mCallback;


    public WLAdapter(Activity pActivity) {
        mActivity = pActivity;
    }

    public void setItemClickCallback(OnWLClickListener pCallback) {
        this.mCallback = pCallback;
    }

    @Override
    public Item onCreateViewHolder(ViewGroup parent, int viewType) {
        View articleView = LayoutInflater.from(parent.getContext()).inflate(R.layout.uc_item, parent, false);
        return new Item(articleView);
    }

    public void addItem(List<WLItem> moreItems) {
        mItems.addAll(moreItems);
        notifyDataSetChanged();
    }

    public void clear(){
        mItems.clear();
    }

    @Override
    public void onBindViewHolder(Item holder, int position) {
        holder.bind(mItems.get(position));
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }


    class Item extends RecyclerView.ViewHolder {

        private TextView tvTitle;
        private TextView tvDate;
        private TextView tvComment;
        private TextView tvUser;

        public Item(View holder) {
            super(holder);
             tvTitle =(TextView) holder.findViewById(R.id.tv_title);
             tvDate =(TextView) holder.findViewById(R.id.tv_date_time);
             tvUser =(TextView) holder.findViewById(R.id.tv_size);
             tvComment =(TextView) holder.findViewById(R.id.tv_comment);
        }

        public void bind(final WLItem ucItem) {
            if (ucItem != null) {

                tvTitle.setText(ucItem.getTitle());
                tvDate.setText(ucItem.getTimestamp());
                tvUser.setText(ucItem.getUser());
                tvComment.setText(ucItem.getComment());

                this.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mCallback != null) {
                            mCallback.onNewsItemItemSelected(ucItem);
                        }
                    }
                });
            }
        }
    }
}

