package com.hpnadig.wikitrack.watchlist;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hpnadig.wikitrack.AppConstants;
import com.hpnadig.wikitrack.R;
import com.hpnadig.wikitrack.WikiTrack;
import com.hpnadig.wikitrack.login.LoginActivity;
import com.hpnadig.wikitrack.revision.ActivityRevision;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


public class WatchListActivity extends AppCompatActivity implements OnWLClickListener, SwipeRefreshLayout.OnRefreshListener{
	

	private WikiTrack app;
	private RecyclerView recyclerView;
	private ProgressBar progressBar;
	private SwipeRefreshLayout refreshLayout;
	private TextView emptyView;
	private TextView footerView;
	List<WLItem> listWL = new ArrayList<>();
    private WLAdapter adapter;
    private LinearLayoutManager layoutManager;

    //For pagination
    private boolean loading = true;
    private int previousTotal = 0;
    private int visibleThreshold = 2;
    private static boolean isRefreshed = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_watchlist);
        app = (WikiTrack)getApplication();

		// Set up the toolbar here
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
		setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Set up the recycler view
        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_layout);
        refreshLayout.setOnRefreshListener(this);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        emptyView = (TextView) findViewById(R.id.emptyView);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        footerView = (TextView) findViewById(R.id.footerView);

        //Set the layout manager
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        //Set the Adapter
        adapter = new WLAdapter(this);
        adapter.setItemClickCallback(this);
        recyclerView.setAdapter(adapter);

        //Set the Scroll listener for the Pagination
        recyclerView.addOnScrollListener(new RecyclerViewScrollListener());

        setCanLoadMore(true);
        makeServerCall();
	}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

	@Override
	protected void onResume() {
		super.onResume();
	}

    private void handleError(final Exception e) {
        Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
    }

    // Scroll listener for the Recylcer view
    class RecyclerViewScrollListener extends RecyclerView.OnScrollListener {

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            Log.d("RRF", "onScrolled");

            refreshLayout.setEnabled(layoutManager.findFirstVisibleItemPosition() == 0);

            int totalItemCount = layoutManager.getItemCount();
            int visibleItemCount = recyclerView.getChildCount();
            int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

            if (loading) {
                if (totalItemCount > previousTotal) {
                    loading = false;
                    previousTotal = totalItemCount;
                }
            }
            if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItemPosition + visibleThreshold)) {
                loading = true;
                footerView.setVisibility(View.VISIBLE);
                makeServerCall();
            }
        }

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }
    }

    /*
     * Show or Hide the footer view as well as the empty view.
     **/
    protected void setCanLoadMore(boolean mCanLoadMore) {
        if (!mCanLoadMore && adapter.getItemCount() == 0) {
            //No items have been retrieved from the server.
            //because of the any reason. may be error has occured or server don't have items
            emptyView.setText(Html.fromHtml(msg_empty_view));
            emptyView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.INVISIBLE);
            footerView.setVisibility(View.GONE);
        } else if (!mCanLoadMore && adapter.getItemCount() > 0) {
            //Unable to retrieve more items from the server.Looks like reached at the end.
            progressBar.setVisibility(View.INVISIBLE);
            footerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.INVISIBLE);
        } else if (mCanLoadMore && adapter.getItemCount() == 0) {
            emptyView.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            footerView.setVisibility(View.GONE);
        } else {
            emptyView.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            footerView.setVisibility(View.GONE);
        }
    }

    private String timestamp;
    private final int LOGIN_REQUEST = 101;
    private String msg_empty_view  = null;
	private void makeServerCall() {

        if (!app.getUserInfoStorage().isLoggedIn()) {
            askForLoginDialog();
            return;
        }

        new WatchListTask(this,app.getPrimarySite(),timestamp){

            @Override
            public void onCatch(Throwable caught) {
                setCanLoadMore(false);
                handleError(new Exception(caught));
            }

            @Override
            public void onFinish(WatchlistResult result) {
                super.onFinish(result);

                if (result.getCode().equals(AppConstants.ERROR_CODE_LOGIN_REQUIRED)) {
                    msg_empty_view = "You must be logged-in to have a watchlist";
                    setCanLoadMore(false);
                    askForLoginDialog();
                    return;
                }

                if (result.getCode().equals("Success")) {
                    String wlJsonData = result.getJsonWatchlist();
                    if (wlJsonData == null) {
                        msg_empty_view = getString(R.string.message_error_no_data_retrived);
                        setCanLoadMore(false);
                        return;
                    }

                    if (wlJsonData.equals("[]")) {
                        String selectedProject = app.getWikiProject();
                        int langIndex = app.findWikiIndex(app.getPrimaryLanguage());
                        String selectedLanguage = app.localNameFor(langIndex);
                        msg_empty_view = "You are not subscribed to any watchlist for project <i>" + selectedProject + "</i> and language <i>" + selectedLanguage + "</i>";
                        setCanLoadMore(false);
                        return;
                    }


                    if (isRefreshed) {
                        isRefreshed = false;
                        refreshLayout.setRefreshing(false);
                        adapter.clear();
                        Snackbar.make(findViewById(R.id.page_container), "Refresh Successfully", Snackbar.LENGTH_SHORT).show();
                    }

                    // Parse the Json data and create the list of objects

                    Type listType = new TypeToken<List<WLItem>>() {
                    }.getType();
                    List<WLItem> listWL = new Gson().fromJson(wlJsonData, listType);

                    if (listWL != null && listWL.size() >= 1) {

                        // If the last timestamp is same, it means the dublicate data is coming.
                        if (timestamp != null && timestamp.equals(listWL.get(listWL.size() - 1).getTimestamp())) {
                            setCanLoadMore(false);
                            return;
                        }
                        timestamp = listWL.get(listWL.size() - 1).getTimestamp();
                        adapter.addItem(listWL);
                        setCanLoadMore(true);
                    } else {
                        setCanLoadMore(false);
                        return;
                    }
                }

            }
        }.execute();
	}

	@Override
	public void onRefresh() {
        isRefreshed = true;
        timestamp = null;
        makeServerCall();
	}

    @Override
    public void onNewsItemItemSelected(WLItem wlItem) {
        Intent startRevisionActivity = new Intent(getApplicationContext(),ActivityRevision.class);
        startRevisionActivity.putExtra(AppConstants.EXTRA_PAGEID, wlItem.getPageid());
        startRevisionActivity.putExtra(AppConstants.EXTRA_REVID, wlItem.getRevid());
        startActivity(startRevisionActivity);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        AppConstants.log("on activity result");
        if (requestCode == LOGIN_REQUEST) {
            if (resultCode == RESULT_OK)
                makeServerCall();
            else
                WatchListActivity.this.finish();
        }
    }

    private void askForLoginDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("You must be logged-in to have a watchlist.");
        builder.setCancelable(false);
        builder.setPositiveButton(
                "Login",
                new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id) {
                        Intent loginIntent = new Intent(WatchListActivity.this, LoginActivity.class);
                        startActivityForResult(loginIntent, LOGIN_REQUEST);
                    }
                });

        builder.setNegativeButton(
                "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        WatchListActivity.this.finish();
                    }
                });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

}
