package com.hpnadig.wikitrack.watchlist;

import android.content.Context;

import com.hpnadig.wikitrack.AppConstants;
import com.hpnadig.wikitrack.Site;
import com.hpnadig.wikitrack.WikiTrack;

import org.json.JSONArray;
import org.mediawiki.api.json.Api;
import org.mediawiki.api.json.ApiResult;
import org.mediawiki.api.json.RequestBuilder;
import org.wikipedia.concurrency.SaneAsyncTask;

public class WatchListTask extends SaneAsyncTask<WatchlistResult>{

    private WikiTrack app;
    private Api api;
    private String timestamp;

    public WatchListTask(Context ctx, Site site, String timestamp)
    {
        super(SINGLE_THREAD);
        app = (WikiTrack)ctx.getApplicationContext();
        api = app.getAPIForSite(site);
        this.timestamp = timestamp;
    }

    @Override
    public WatchlistResult performTask() throws Throwable {
        try {
            RequestBuilder rb = api.action("query")
                    .param("list", "watchlist")
                    .param("wlprop", "user|comment|timestamp|ids|title")
                    .param("format","json");
            if(timestamp!=null)
                rb.param("wlstart",timestamp);

            ApiResult result = rb.get();
            AppConstants.log("jsonwatchlist from server "+ result.asObject());

            if (result.asObject().optJSONObject("error") != null) {
                String code = result.asObject().optJSONObject("error").optString("code");
                return new WatchlistResult(code, null);
            }

            JSONArray jsonWatchlist = result.asObject().optJSONObject("query").optJSONArray("watchlist");
            return new WatchlistResult("Success", jsonWatchlist.toString());
        } catch (NullPointerException e) {
            AppConstants.logE("Error", e);
            return new WatchlistResult(null);
        }
    }

}
