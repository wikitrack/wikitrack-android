package com.hpnadig.wikitrack.watchlist;


public class WatchlistResult {
	private String jsonWatchlist;
	private String code;

	public WatchlistResult(String code, String jsonWatchlist) {
		this.jsonWatchlist = jsonWatchlist;
		this.code = code;
	}

	public WatchlistResult(String jsonWatchlist) {
			this.jsonWatchlist = jsonWatchlist;
	}
	
	public String getJsonWatchlist() {
		return jsonWatchlist;
	}
	
	public void setJsonWatchlist(String jsonWatchlist) {
		this.jsonWatchlist = jsonWatchlist;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
