package org.wikipedia.settings;

import org.wikipedia.settings.LanguagePreference;
import org.wikipedia.settings.PreferenceActivityWithBack;

import com.hpnadig.wikitrack.R;
import com.hpnadig.wikitrack.WikiTrack;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceManager;

public class SettingsActivity extends PreferenceActivityWithBack implements SharedPreferences.OnSharedPreferenceChangeListener {
    public static final int ACTIVITY_RESULT_LANGUAGE_CHANGED = 1;
    public static final int ACTIVITY_RESULT_LOGOUT = 2;
    public static final int ACTIVITY_REQUEST_SHOW_SETTINGS = 1;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }

    @Override
    protected void onStop() {
        super.onStop();
        PreferenceManager.getDefaultSharedPreferences(this).unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(WikiTrack.PREFERENCE_CONTENT_LANGUAGE)) {
            LanguagePreference pref = (LanguagePreference) findPreference(WikiTrack.PREFERENCE_CONTENT_LANGUAGE);
            pref.setSummary(pref.getCurrentLanguageDisplayString());
            setResult(ACTIVITY_RESULT_LANGUAGE_CHANGED);
        }
    }
}